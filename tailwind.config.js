module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontSize: {
        sm: ["14px", "20px"],
        base: ["18px", "24px"],
        lg: ["20px", "28px"],
        xl: ["24px", "32px"],
      },
      transformOrigin: {
        0: "0%",
      },
      fontFamily: {
        uchen: ["uchen", "sans-serif"],
      },
    },
  },
  plugins: [require("tailwind-scrollbar-hide")],
};
