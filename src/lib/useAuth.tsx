import {
  ApolloClient,
  ApolloProvider,
  HttpLink,
  InMemoryCache,
} from "@apollo/client";
import { NormalizedCacheObject } from "apollo-cache-inmemory"; 
import React, {
  createContext,
  Dispatch,
  useContext,
  useEffect,
  useState,
} from "react";
import { REFRESH_TOKEN_MUTATION, GET_AUTH_USER } from "../graphql/queries/auth";
import { createUploadLink } from "apollo-upload-client";
import { User } from "../common/global-types";
import Cookies from "js-cookie";
import toast from "react-hot-toast";
import { Buffer } from "buffer";
window.Buffer = window.Buffer || Buffer;

interface IAuthContext {
  createAuthApolloClient: () => ApolloClient<NormalizedCacheObject>;
  createPostApolloClient: () => ApolloClient<NormalizedCacheObject>;
  authAccessToken: string | null;
  authRefreshToken: string | null;
  setAuthAccessToken: Dispatch<React.SetStateAction<string | null>>;
  setAuthRefreshToken: Dispatch<React.SetStateAction<string | null>>;
  logout: () => void;
  isAuthenticated: () => boolean;
  setAccessToken: (token: string) => void;
  setRefreshToken: (token: string) => void;
  authUser: User | null;
  setAuthUser: Dispatch<React.SetStateAction<User | null>>;
  clearAuthUser: () => void;
}

interface IAuthProviderProps {
  children?: React.ReactNode;
}

const AuthContext = createContext({} as IAuthContext);

export const AuthProvider: React.FC<IAuthProviderProps> = ({ children }) => {
  const auth = useProvideAuth();
  return (
    <AuthContext.Provider value={auth}>
      <ApolloProvider client={auth.createAuthApolloClient()}>
        {children}
      </ApolloProvider>
    </AuthContext.Provider>
  );
};

// get access token from local storage
export const getAccessTokenFromCookie = () => {
  return Cookies.get("accessToken");
};

// get refresh token from cookies
export const getRefreshTokenFromCookie = () => {
  return Cookies.get("refreshToken");
};

const isTokenExpired = (token: string) => {
  try {
    const expiry = JSON.parse(
      Buffer.from(token.split(".")[1], "base64").toString()
    ).exp;
    return Math.floor(new Date().getTime() / 1000) >= expiry;
  } catch (error) {
    console.log("error checking expired status", error);
    return false;
  }
};

// Get access token
export const getAccessToken = async (
  accessToken: string | undefined,
  refreshToken: string | undefined
): Promise<string | null> => {
  if (accessToken) {
    return accessToken;
  }

  if (refreshToken) {
    // refresh the token if expired
    const refreshTokenClient = new ApolloClient({
      link: new HttpLink({
        uri: process.env.REACT_APP_BACKEND_URL,
        headers: { "x-auth-refresh-token": refreshToken },
      }),
      cache: new InMemoryCache(),
    });

    return new Promise((resolve) => {
      refreshTokenClient
        .mutate({ mutation: REFRESH_TOKEN_MUTATION })
        .then((response) => {
          const { accessToken } = response.data.refreshToken;
          resolve(accessToken);
        })
        .catch(() => {
          resolve(null);
        });
    });
  }

  return null;
};

export const getAuthUser = async (
  token: string | undefined
): Promise<User | null> => {
  const client = new ApolloClient({
    link: new HttpLink({
      uri: process.env.REACT_APP_BACKEND_URL,
      headers: { Authorization: `Bearer ${token}` },
    }),
    cache: new InMemoryCache(),
  });
  return new Promise((resolve) => {
    client
      .query({ query: GET_AUTH_USER })
      .then((response) => {
        //console.log("response: ", response.data);
        resolve(response.data.me);
      })
      .catch(() => {
        resolve(null);
      });
  });
};

export const useProvideAuth = () => {
  const [authAccessToken, setAuthAccessToken] = useState<string | null>(null);
  const [authRefreshToken, setAuthRefreshToken] = useState<string | null>(null);
  const [authUser, setAuthUser] = useState<User | null>(null);

  // setAccessToken
  const setAccessToken = (token: string) => {
    setAuthAccessToken(token);
    Cookies.set("accessToken", token, {
      sameSite: "strict",
      secure: true,
      expires: new Date(new Date().getTime() + 15 * 60 * 1000), // expires in 15 minutes
    });
  };

  // setRefreshToken
  const setRefreshToken = (token: string) => {
    setAuthRefreshToken(token);
    Cookies.set("refreshToken", token, {
      sameSite: "strict",
      secure: true,
      expires: new Date(new Date().getTime() + 7 * 24 * 60 * 60 * 1000), //expires in 7 days
    });
  };

  useEffect(() => {
    const initAccessToken = async () => {
      // console.log("init access token");
      const accessToken = getAccessTokenFromCookie();
      const refreshToken = getRefreshTokenFromCookie();
      let token;
      if (!accessToken || isTokenExpired(accessToken as string)) {
        // refresh the token if expired
        token = await getAccessToken(undefined, refreshToken);
      } else {
        token = accessToken;
      }
      const user = await getAuthUser(token as string);
      if (user) {
        setAuthUser(user);
      }
      if (token) {
        setAccessToken(token);
      }
    };

    initAccessToken();
  }, []);

  useEffect(() => {
    const silentAuth = async () => {
      const accessToken = getAccessTokenFromCookie();
      const refreshToken = getRefreshTokenFromCookie();
      let token;
      // console.log(isTokenExpired(accessToken as string));
      if (!accessToken || isTokenExpired(accessToken as string)) {
        // refresh the token if expired
        token = await getAccessToken(undefined, refreshToken);
        // get the auth user
        const user = await getAuthUser(token as string);
        if (user) {
          setAccessToken(token as string);
          // set the auth user
          setAuthUser(user);
          return;
        }
        setAuthUser(null);
      }
    };

    // silently refresh every 1 minute
    const silentAuthIntervalId = setInterval(() => {
      silentAuth();
    }, 4 * 60 * 1000);

    return () => clearInterval(silentAuthIntervalId);
  });

  // Get auth headers
  const getAuthHeaders = () => {
    if (!authAccessToken) {
      return null;
    }

    return {
      Authorization: `Bearer ${authAccessToken}`,
    };
  };

  // Create a new Apollo client instance (with the auth header)
  const createAuthApolloClient = () => {
    const link = new HttpLink({
      uri: process.env.REACT_APP_BACKEND_URL,
      headers: getAuthHeaders(),
    });

    const client = new ApolloClient({
      link,
      cache: new InMemoryCache(),
    });
    return client;
  };

  // This uses the apollo-upload-client link to support file uploads
  const createPostApolloClient = () => {
    const link = createUploadLink({
      uri: process.env.REACT_APP_BACKEND_URL,
      headers: getAuthHeaders(),
    });

    const client = new ApolloClient({
      link,
      cache: new InMemoryCache(),
    });
    return client;
  };

  // Check if user is signed in
  const isAuthenticated = () => {
    return !(authUser === null);
  };

  // Clear auth user
  const clearAuthUser = () => {
    Cookies.remove("accessToken", {
      secure: true,
      expires: new Date(new Date().getTime() + 15 * 60 * 1000), // expires in 15 minutes
    });
    Cookies.remove("refreshToken", {
      sameSite: "strict",
      secure: true,
      expires: new Date(new Date().getTime() + 7 * 24 * 60 * 1000), //expires in 7 days});
    });
    setAuthAccessToken(null);
    setAuthRefreshToken(null);
    setAuthUser(null);
  }

  // Logout
  const logout = () => {
    clearAuthUser()

    toast.success("Log out successful!");
  };

  return {
    createAuthApolloClient,
    createPostApolloClient,
    authAccessToken,
    authRefreshToken,
    setAuthAccessToken,
    setAuthRefreshToken,
    setAccessToken,
    setRefreshToken,
    logout,
    isAuthenticated,
    authUser,
    setAuthUser,
    clearAuthUser,
  };
};

export const useAuth = () => useContext(AuthContext);
