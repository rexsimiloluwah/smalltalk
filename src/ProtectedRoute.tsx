import React from "react";
import { Navigate } from "react-router-dom";
import { getAccessTokenFromCookie } from "./lib/useAuth";

interface IProtectedRouteProps {
  redirectPath?: string;
  children: React.ReactNode;
}

const ProtectedRoute: React.FC<IProtectedRouteProps> = ({
  redirectPath,
  children,
}) => {
  const token = getAccessTokenFromCookie();

  return token ? (
    <div>{children}</div>
  ) : (
    <Navigate to={redirectPath as string} />
  );
};

export default ProtectedRoute;

ProtectedRoute.defaultProps = {
  redirectPath: "/login",
};
