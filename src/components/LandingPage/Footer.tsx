import React from "react";
import { Link } from "react-router-dom";
import { BsEnvelope, BsFacebook, BsInstagram, BsTwitter } from "react-icons/bs";
import { Container, Divider } from "../shared";
import Logo from "../shared/Logo";

const socialIcons = [
  {
    title: "Mail",
    icon: <BsEnvelope size={24} />,
    url: "",
  },
  {
    title: "Facebook",
    icon: <BsFacebook size={24} />,
    url: "",
  },
  {
    title: "Twitter",
    icon: <BsTwitter size={24} />,
    url: "",
  },
  {
    title: "Instagram",
    icon: <BsInstagram size={24} />,
    url: "",
  },
];

const footerNavigation = [
  {
    title: "Join Smalltalk",
    url: "/signup",
    section: "",
  },
  {
    title: "About",
    url: null,
    section: "about_section",
  },
  {
    title: "Feed",
    url: "/feed",
    section: "",
  },
  {
    title: "Contact",
    url: "/contact",
    section: "",
  },
];

const supportNavigation = [
  {
    title: "Report an Issue/Bug",
    url: null,
    section: "",
  },
  {
    title: "Stay Updated!",
    url: null,
    section: "stay_updated_section",
  },
  {
    title: "Privacy Policy",
    url: null,
    section: "",
  },
  {
    title: "Contribute",
    url: null,
    section: "",
  },
];

const Footer: React.FC = () => {
  // scroll to a particular section on the page
  const handleScrollToSection = (section: string) => {
    // get the location of the section
    const location = document.getElementById(section)?.offsetTop;
    const navbarHeight = document.getElementById("navbar")?.offsetHeight;
    // scroll to the location
    window.scrollTo({
      left: 0,
      top: Number(location) - Number(navbarHeight) - 20, // offset by height of the navbar
    });
  };
  return (
    <div className="bg-fuchsia-200">
      <Container otherStyles="space-y-6">
        <div>
          <div className="flex justify-between md:flex-row space-y-4 md:space-y-0 flex-col">
            <div>
              <Logo otherStyles="inline-flex flex-start" />
              <p className="text-gray-500">Talk about Everything!</p>
              <ul className="inline-flex space-x-5 items-center my-2">
                {socialIcons.map((item, i) => (
                  <li className="cursor-pointer hover:text-indigo-500" key={i}>
                    {item.icon}
                  </li>
                ))}
              </ul>
            </div>
            <div className="grid grid-cols-2 gap-12">
              <div>
                <h1 className="font-bold text-[18px]">Navigation</h1>
                <ul>
                  {footerNavigation.map((item, i) => (
                    <li
                      className="cursor-pointer hover:text-indigo-500 text-base text-gray-700"
                      key={i}
                    >
                      {item.url ? (
                        <Link to={item.url}>{item.title}</Link>
                      ) : (
                        <span
                          onClick={() => handleScrollToSection(item.section)}
                        >
                          {item.title}
                        </span>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
              <div>
                <h1 className="font-bold text-[18px]">Support</h1>
                <ul>
                  {supportNavigation.map((item, i) => (
                    <li
                      className="cursor-pointer hover:text-indigo-500 text-base text-gray-700"
                      key={i}
                    >
                      {item.url ? (
                        <Link to={item.url}>{item.title}</Link>
                      ) : (
                        <span
                          onClick={() => handleScrollToSection(item.section)}
                        >
                          {item.title}
                        </span>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
        <Divider />
        <div className="flex justify-between text-gray-700">
          <div>
            <p>©️{new Date().getFullYear()} SmallTalk</p>
          </div>
          <span className="max-w-md">
            Built with <span aria-label="img">❤️️</span>
          </span>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
