export { default as Footer } from "./Footer";
export { default as Hero } from "./Hero";
export { default as StayUpdated } from "./StayUpdated";
export { default as About } from "./About";
