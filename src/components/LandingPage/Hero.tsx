import React, { useEffect, useState } from "react";
import { Button, Container } from "../shared";

const Hero: React.FC = () => {
  const topics = [
    "Finance",
    "Mental Health",
    "Science",
    "Tech",
    "Entertainment",
    "Fashion",
    "Football",
    "Politics",
    "Nigeria",
    "Everything",
  ];
  const [currentTopicIndex, setCurrentTopicIndex] = useState<number>(0);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setCurrentTopicIndex(
        currentTopicIndex >= topics.length - 1 ? 0 : currentTopicIndex + 1
      );
    }, 3000);

    return () => clearTimeout(timeoutId);
  });

  return (
    <div className="bg-gradient-light relative">
      <Container>
        <div className="h-[400px] relative flex flex-col space-y-4 text-center justify-center items-center w-full ">
          <h1 className="text-5xl font-bold">Small Talk</h1>
          <p className="text-3xl text-gray-600">
            Talk about{" "}
            <span className="inline-block font-semibold show">
              {topics[currentTopicIndex]}
            </span>
          </p>
          <Button
            label="Get Started"
            otherStyles="py-4 px-8 mt-2 animate-pulse hover:animate-none text-lg"
            href="/signup"
            name="signup"
          />
          <div className="bg-fuchsia-300 show border-2 hover:rotate-45 duration-300 border-indigo-500 border-dotted w-24 h-24 absolute md:top-8 md:left-24 top-0 left-8 rounded-full my-auto items-center pulse">
            <img
              src="/images/memoji-dog.png"
              alt="dog-1"
              className="w-full h-full object-fill"
            />
          </div>
          <div className="bg-fuchsia-300 show border-4 hover:-rotate-45 duration-300 border-indigo-500 w-24 h-24 absolute md:top-8 md:right-24 top-0 right-8 rounded-full pulse">
            <img
              src="/images/memoji-girl.png"
              alt="girl-1"
              className="w-full h-full object-fill"
            />
          </div>
          <div className="bg-fuchsia-300 show border-4 hover:rotate-45 duration-300 border-indigo-500 w-24 h-24 absolute md:bottom-8 md:left-44 bottom-0 left-8 rounded-full pulse">
            <img
              src="/images/memoji-man.png"
              alt="man-1"
              className="w-full h-full object-fill"
            />
          </div>
          <div className="bg-fuchsia-300 show border-2 hover:-rotate-45 duration-300 border-indigo-500 border-dotted w-24 h-24 absolute md:bottom-8 md:right-44 bottom-0 right-8 rounded-full pulse">
            <img
              src="/images/memoji-man-2.png"
              alt="man-2"
              className="w-full h-full object-fill"
            />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Hero;
