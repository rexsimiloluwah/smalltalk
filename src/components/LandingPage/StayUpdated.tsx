import React, { useState } from "react";
import { Button, Container, Modal } from "../shared";
import * as Yup from "yup";

const StayUpdated: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [isSubscribeSuccess, setIsSubscribeSuccess] = useState<boolean>(false);

  const handleSubscription = () => {
    setIsSubscribeSuccess(true);
  };
  return (
    <>
      <Modal
        isOpen={isSubscribeSuccess}
        handleClose={() => setIsSubscribeSuccess(!isSubscribeSuccess)}
        title=""
      >
        <div className="flex justify-center items-center flex-col space-y-3">
          <div className="bg-fuchsia-300 border-2 border-indigo-500 border-dotted rounded-full w-24 h-24">
            <img
              src="/images/memoji-man-2.png"
              width={100}
              height={100}
              alt="man-2"
            />
          </div>
          <h1 className="text-2xl font-semibold">Thanks for Subscribing</h1>
          <p className="text-gray-700">
            I promise to keep you updated! <span aria-label="img">😉</span>
          </p>
        </div>
      </Modal>
      <Container
        otherStyles="bg-gradient-light rounded-lg shadow-lg space-y-5 mb-24 mt-16 border-[1px] border-gray-500"
        id="stay_updated_section"
      >
        <div className="text-center space-y-3">
          <h1 className="text-4xl text-indigo-500 font-bold">Stay Updated</h1>
          <p className="text-gray-500 text-lg">
            Be the first to when new features are released!
          </p>
        </div>
        <div className="m-auto flex justify-center bg-white w-[95%] md:w-[80%] rounded-xl border-[1px] border-gray-500">
          <input
            type="text"
            className="flex-1 py-3 px-2 rounded-xl"
            placeholder="Enter E-mail Address"
            onChange={(e) => setEmail(e.target.value)}
          />
          <Button
            label="Subscribe"
            disabled={!Yup.string().email().required().isValidSync(email)}
            otherStyles="p-3 my-2 mx-3"
            handleClick={handleSubscription}
          />
        </div>
      </Container>
    </>
  );
};

export default StayUpdated;
