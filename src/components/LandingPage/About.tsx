import React, { useState } from "react";
import { DemoPostCard } from "../Post";
import { Post, User } from "../../common/global-types";
import { Button, Container, Slider } from "../shared";
import { useBottomPlayerContext } from "../../context";

const dummyPosts: Post[] = [
  {
    id: 1,
    creator: {
      firstname: "Similoluwa",
      lastname: "Okunowo",
      username: "theblackdove",
    } as User,
    creatorId: 1,
    category: "",
    tags: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    title: "Testing Smalltalk",
    description: "Testing Smalltalk description",
    audioFileUrl: "/audio/audio1.mp3",
    likes: [],
    comments: [],
  },
  {
    id: 1,
    creator: {
      firstname: "Similoluwa",
      lastname: "Okunowo",
      username: "theblackdove",
    } as User,
    creatorId: 1,
    category: "",
    tags: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    title: "Testing Smalltalk",
    description: "Testing Smalltalk description",
    audioFileUrl: "/audio/audio__coffin.mp3",
    likes: [],
    comments: [],
  },
  {
    id: 1,
    creator: {
      firstname: "Similoluwa",
      lastname: "Okunowo",
      username: "theblackdove",
    } as User,
    creatorId: 1,
    category: "",
    tags: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    title: "Testing Smalltalk",
    description: "Testing Smalltalk description",
    audioFileUrl: "/audio/helloworld.mp3",
    likes: [],
    comments: [],
  },
  {
    id: 1,
    creator: {
      firstname: "Similoluwa",
      lastname: "Okunowo",
      username: "theblackdove",
    } as User,
    creatorId: 1,
    category: "",
    tags: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    title: "Testing Smalltalk",
    description: "Testing Smalltalk description",
    audioFileUrl: "/audio/audio2.mp3",
    likes: [],
    comments: [],
  },
];

const About: React.FC = () => {
  // number of cards to be shown in the slider
  const NUMBER_OF_CARDS = window.innerWidth > 768 ? 2 : 1;
  // const [activeItemIdx, setActiveItemIdx] = useState<number>(0);
  const { setPlayerStatus, setCurrentTrackMeta, setAudioSrc, playerStatus } =
    useBottomPlayerContext();

  // TODO: auto move the slider cards
  // const onChangeItem = (value: number) => {
  //   setActiveItemIdx(value);
  // };

  // const tick = () => {
  //   setActiveItemIdx(
  //     (prev) => (prev + 1) % (dummyPosts.length - noOfCards + 1)
  //   );
  // };

  // useEffect(() => {
  //   const interval = setInterval(tick, 1000);

  //   return clearInterval(interval);
  // });

  const [currentPostPlayingIdx, setCurrentPostPlayingIdx] = useState<
    number | null
  >(null);

  const carouselItems = dummyPosts.map((post, i) => (
    <DemoPostCard
      post={post}
      key={i}
      isPlaying={currentPostPlayingIdx === i && playerStatus.isPlaying}
      isPaused={
        currentPostPlayingIdx === i &&
        playerStatus.isActive &&
        !playerStatus.isPlaying
      }
      handlePlay={() => {
        setCurrentPostPlayingIdx(i);
        setPlayerStatus({
          isPlaying: true,
          isActive: true,
          isOpen: true,
        });
        setCurrentTrackMeta({
          title: post.title as string,
          topic: post.category as string,
          username: post.creator?.username,
        });
        setAudioSrc(post.audioFileUrl as string);
      }}
      handlePause={() => {
        setPlayerStatus((prev) => ({ ...prev, isPlaying: false }));
      }}
    />
  ));

  return (
    <Container otherStyles="my-3 space-y-5" id="about_section">
      <div className="space-y-4 text-center">
        <h1 className="text-4xl text-indigo-500 font-bold text-center">
          Talk is <span className="line-through">Cheap</span> Amazing!
        </h1>
        <p className="text-gray-500 text-lg">
          Unlock a seamless talking experience with SmallTalk
        </p>
      </div>
      <Slider noOfItems={NUMBER_OF_CARDS} sliderItems={carouselItems} />
      <Button
        name="go_to_feed"
        label="Go to Feed"
        otherStyles="px-6 py-3 mx-auto mt-3"
        href="/feed"
      />
    </Container>
  );
};

export default About;
