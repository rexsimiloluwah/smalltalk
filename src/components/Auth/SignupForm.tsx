import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { FormInput, Button, FormRow } from "../shared";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import { MdOutlineAlternateEmail } from "react-icons/md";
import { RiUser3Line, RiLockPasswordLine } from "react-icons/ri";
import { REGISTER_USER_MUTATION } from "../../graphql/queries/auth";
import toast, { Toaster } from "react-hot-toast";

const SignupForm: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const [register] = useMutation(REGISTER_USER_MUTATION);

  const formik = useFormik({
    initialValues: {
      email: "",
      firstname: "",
      lastname: "",
      password: "",
      username: "",
    },
    onSubmit: async (values) => {
      const { email, firstname, lastname, username, password } = values;
      setLoading(true);
      await register({
        variables: {
          data: {
            email: email,
            firstname: firstname,
            lastname: lastname,
            username: username,
            password: password,
          },
        },
      })
        .then(() => {
          setLoading(false);
          toast.success("Sign Up Successful! 🎉");
          navigate("/login");
        })
        .catch((error) => {
          setLoading(false);
          toast.error(`Sign up failed! ${error}`);
        });
    },
    validationSchema: Yup.object({
      email: Yup.string().email().required().label("Email"),
      username: Yup.string().required().min(2).label("Username"),
      firstname: Yup.string().required().min(2).label("Firstname"),
      lastname: Yup.string().required().min(2).label("Lastname"),
      password: Yup.string()
        .required()
        .min(8, "Password must be longer than 8 characters.")
        .label("Password"),
    }),
  });

  return (
    <>
      <Toaster />
      <div className="flex items-center flex-col w-[80%] justify-center m-auto min-h-screen font-uchen">
        <div className="mb-4 text-center">
          <h1 className="font-bold text-xl md:text-3xl">Sign Up</h1>
          <p className="text-gray-500">Join Smalltalk</p>
        </div>
        <form
          onSubmit={formik.handleSubmit}
          className="flex flex-col space-y-3 w-full"
        >
          <FormRow>
            <FormInput
              name="firstname"
              id="firstname"
              placeholder="Enter Firstname"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.firstname}
              error={formik.errors.firstname}
              addOn={true}
              addOnIcon={<RiUser3Line />}
              otherStyles="bg-fuchsia-50"
            />
            <FormInput
              name="lastname"
              id="lastname"
              placeholder="Enter Lastname"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.lastname}
              error={formik.errors.lastname}
              addOn={true}
              addOnIcon={<RiUser3Line />}
              otherStyles="bg-fuchsia-50"
            />
          </FormRow>
          <FormInput
            name="email"
            id="email"
            placeholder="Enter E-mail"
            handleChange={formik.handleChange}
            handleBlur={formik.handleBlur}
            touched={formik.touched.email}
            error={formik.errors.email}
            addOn={true}
            addOnIcon={<MdOutlineAlternateEmail />}
            otherStyles="bg-fuchsia-50"
          />
          <FormInput
            name="username"
            id="username"
            placeholder="Enter Username"
            handleChange={formik.handleChange}
            handleBlur={formik.handleBlur}
            touched={formik.touched.username}
            error={formik.errors.username}
            addOn={true}
            addOnIcon={<RiUser3Line />}
            otherStyles="bg-fuchsia-50"
          />
          <FormInput
            type={"password"}
            name="password"
            id="password"
            placeholder="Enter Password"
            handleChange={formik.handleChange}
            handleBlur={formik.handleBlur}
            touched={formik.touched.password}
            error={formik.errors.password}
            addOn={true}
            addOnIcon={<RiLockPasswordLine />}
            otherStyles="bg-fuchsia-50"
          />
          <Button
            name="signup"
            type="submit"
            label="Sign Up"
            loading={loading}
            loadingText="Signing Up..."
            otherStyles="p-3"
            disabled={!(formik.dirty && formik.isValid)}
          />
        </form>
      </div>
    </>
  );
};

export default SignupForm;
