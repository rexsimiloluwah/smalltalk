import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";
import { useAuth, getAuthUser } from "../../lib/useAuth";
import { GoogleLogin, CredentialResponse } from "@react-oauth/google";
import { Button, FormInput } from "../shared";
import { useFormik } from "formik";
import * as Yup from "yup";
import { MdOutlineAlternateEmail } from "react-icons/md";
import { RiLockPasswordLine } from "react-icons/ri";
import {
  LOGIN_MUTATION,
  LOGIN_WITH_GOOGLE_MUTATION,
} from "../../graphql/queries/auth";

const LoginForm: React.FC = () => {
  const navigate = useNavigate();
  const { setAccessToken, setRefreshToken, setAuthUser } = useAuth();
  const [loading, setLoading] = useState<boolean>(false);

  const [loginWithEmail] = useMutation(LOGIN_MUTATION);

  const [loginWithGoogle] = useMutation(LOGIN_WITH_GOOGLE_MUTATION);

  const handleLoginWithGoogle = async (
    credentialResponse: CredentialResponse
  ) => {
    const { credential } = credentialResponse;
    try {
      setLoading(true);
      const { data } = await loginWithGoogle({
        variables: {
          data: {
            token: credential,
          },
        },
      });
      const { accessToken, refreshToken } = data.loginWithGoogle.token;
      setAccessToken(accessToken);
      setRefreshToken(refreshToken);
      setLoading(false);
      toast.success("Log In Successful! 🎉");
      const user = await getAuthUser(accessToken);
      setAuthUser(user);
      navigate("/feed");
    } catch (error) {
      setLoading(false);
      toast.error(`Login failed! ${error}`);
    }
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    onSubmit: async (values) => {
      const { email, password } = values;
      try {
        setLoading(true);
        const { data } = await loginWithEmail({
          variables: {
            data: {
              email: email,
              password: password,
            },
          },
        });
        const { accessToken, refreshToken } = data.login.token;
        setAccessToken(accessToken);
        setRefreshToken(refreshToken);
        setLoading(false);
        toast.success("Log In Successful! 🎉");
        const user = await getAuthUser(accessToken);
        setAuthUser(user);
        // console.log("navigating to feed");
        setTimeout(() => navigate("/feed"), 1000);
      } catch (error) {
        setLoading(false);
        toast.error(`Login failed! ${error}`);
      }
    },
    validationSchema: Yup.object({
      email: Yup.string().email().required().label("Email"),
      password: Yup.string()
        .required()
        .min(8, "Password must be longer than 8 characters.")
        .label("Password"),
    }),
  });

  return (
    <>
      <Toaster />
      <div className="m-auto min-h-screen flex items-center justify-center flex-col font-uchen">
        <div className="w-[90%] md:w-[30%] shadow-lg bg-gradient-light py-5 px-8 rounded-xl flex flex-col justify-center">
          <h1 className="text-center text-xl md:text-3xl font-bold my-2">
            Login
          </h1>
          <form
            className="flex flex-col space-y-3 w-full justify-center"
            onSubmit={formik.handleSubmit}
          >
            <FormInput
              name="email"
              id="email"
              placeholder="Enter E-mail"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.email}
              error={formik.errors.email}
              addOn={true}
              addOnIcon={<MdOutlineAlternateEmail />}
            />

            <FormInput
              type={"password"}
              name="password"
              id="password"
              placeholder="Enter Password"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.password}
              error={formik.errors.password}
              addOn={true}
              addOnIcon={<RiLockPasswordLine />}
            />
            <Button
              name="login"
              type="submit"
              label="Log In"
              loadingText="Logging In..."
              otherStyles="p-3"
              loading={loading}
              disabled={!(formik.dirty && formik.isValid)}
            />
            <div className="flex text-[15px] justify-between">
              <Link to="/forgot-password">
                <span className="hover:underline hover:cursor-pointer">
                  Forgot Password
                </span>
              </Link>
              <Link to="/signup">
                <span className="hover:underline hover:cursor-pointer">
                  No Account? Sign Up
                </span>
              </Link>
            </div>
          </form>
          <div className="relative flex py-2 items-center">
            <div className="flex-grow border-t border-gray-400"></div>
            <span className="flex-shrink mx-4 text-gray-400">OR</span>
            <div className="flex-grow border-t border-gray-400"></div>
          </div>
          <div className="flex justify-center">
            <GoogleLogin
              onSuccess={(credentialResponse) => {
                handleLoginWithGoogle(credentialResponse);
              }}
              onError={() => {
                toast.error("Login Failed!");
              }}
              useOneTap
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
