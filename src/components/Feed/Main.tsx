import React, { useState, useRef, useEffect } from "react";
import SearchInput from "./SearchInput";
import FeedPosts from "./FeedPosts";
import { PaginatedResult, Post } from "../../common/global-types";
import { useSearchInputContext } from "../../context/SearchInputContext";
import { Button } from "../shared";
import { GET_AUTH_USER_FEED } from "../../graphql/queries/feed";
import { useAuth } from "../../lib/useAuth";
import { DEFAULT_FEED_PAGE_SIZE } from "../../common/constants";
import { Audio } from "react-loader-spinner";

interface IMainProps {
  feed: PaginatedResult<Post> | null;
  topic: string;
}

const Main: React.FC<IMainProps> = ({ feed, topic }) => {
  const { searchActive } = useSearchInputContext();
  const [searchText, setSearchText] = useState<string>("");
  const currentPage = useRef<number>(Number(feed?.currentPage));
  const [feedState, setFeedState] = useState<PaginatedResult<Post> | null>(
    feed
  );
  const [filteredFeedPosts, setFilteredFeedPosts] = useState<Post[]>(
    feed?.data as Post[]
  );
  const [loadingMore, setLoadingMore] = useState<boolean>(false);
  const { createAuthApolloClient } = useAuth();
  const client = createAuthApolloClient();

  useEffect(() => {
    setFilteredFeedPosts(feed?.data as Post[]);
    setFeedState(feed);
  }, [feed]);

  // fetch more posts - for paginated feed
  const fetchMorePosts = async () => {
    setLoadingMore(true);
    await client
      .query({
        query: GET_AUTH_USER_FEED,
        variables: {
          args: {
            pageSize: DEFAULT_FEED_PAGE_SIZE,
            currentPage: Number(feed?.currentPage) + 1,
          },
          where: {
            topic: topic,
          },
        },
      })
      .then((data) => {
        const feed = data.data.userFeed as PaginatedResult<Post>;
        // update the feed state
        setFeedState((prev) => ({
          ...feed,
          data: [...(prev?.data as Post[]), ...feed.data],
        }));
        setFilteredFeedPosts((prev) => [...prev, ...feed.data]);
        currentPage.current += 1;
        setLoadingMore(false);
      })
      .catch((error) => {
        console.error(error);
        setLoadingMore(false);
      });
  };

  // TODO: Update this search function to handle for different search modes
  // i.e. search by topic, tag, people
  const handleSearch = (searchString: string) => {
    setSearchText(searchString);
    setFilteredFeedPosts(
      (feedState?.data as Post[]).filter((post) => {
        return (
          post.title?.toLowerCase().includes(searchString) ||
          post.category?.toLowerCase().includes(searchString)
        );
      })
    );
  };

  if (!filteredFeedPosts) {
    return (
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50 flex justify-center items-center">
        <Audio color="fuchsia" height={40} />
      </div>
    );
  }

  if (filteredFeedPosts.length === 0) {
    return (
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50">
        <div className="search border-b-[1px] py-4 px-8 sticky top-0 bg-white z-50">
          <SearchInput handleSearch={handleSearch} />
        </div>
        <div className="md:px-8 px-4 py-4">
          <div className="flex justify-center flex-col my-4 place-items-center space-y-6 text-center">
            <img
              src={"/images/audio_player.svg"}
              alt="feed_empty"
              width={300}
              height={300}
            />
            <h1 className="text-xl md:text-3xl font-semibold text-indigo-500 max-w-3xl">
              {searchActive && searchText !== ""
                ? `No post matches the search query "${searchText}"`
                : "Oops! Your feed is empty"}
            </h1>
          </div>
        </div>
      </div>
    );
  }
  return (
    <>
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50">
        <div className="search border-b-[1px] py-4 px-8 sticky top-0 bg-white z-50">
          <SearchInput handleSearch={handleSearch} />
        </div>
        <div className="md:px-8 px-4 py-4">
          <FeedPosts feed={filteredFeedPosts} />
          {feedState?.hasNextPage && (
            <Button
              label="Load More"
              loading={loadingMore}
              loadingText="Loading More Posts..."
              otherStyles="m-auto py-2 px-4"
              handleClick={fetchMorePosts}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Main;
