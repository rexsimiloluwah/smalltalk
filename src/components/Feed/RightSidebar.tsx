import { useQuery } from "@apollo/client";
import { Link, useNavigate } from "react-router-dom";
import React from "react";
import { User } from "../../common/global-types";
import { GET_USERS } from "../../graphql/queries/user";
import { useAuth } from "../../lib/useAuth";
import { Divider, UserPill } from "../shared";

const popularCategories = [
  "Health",
  "Programming",
  "JavaScript",
  "AI",
  "Politics",
  "Nigeria",
  "Russia vs. Ukraine",
  "UK",
  "Depression",
];
const RightSidebar: React.FC = () => {
  const navigate = useNavigate();
  const { data } = useQuery(GET_USERS, { variables: { where: {} } });
  const { authUser } = useAuth();

  return (
    <div className="hidden h-screen border-l-[1px] border-gray-500 md:block w-[18rem] relative px-4 py-6 space-y-6">
      <div>
        <h1 className="text-2xl text-indigo-600">Popular Categories</h1>
        <Divider />
        <div className="flex flex-wrap gap-1">
          {popularCategories.map((category, id) => (
            <Link to={`/feed?topic=${category.toLowerCase()}`} key={id}>
              <button
                className="border-[1px] border-gray-500 p-2 rounded-xl hover:border-indigo-500 hover:text-indigo-500 hover:font-semibold hover:border-2"
                name={`topic_${category}`}
              >
                {category}
              </button>
            </Link>
          ))}
        </div>
      </div>
      <div>
        <h1 className="text-2xl text-indigo-500">People to Follow</h1>
        <Divider />
        <div className="border-[1px] border-gray-500 p-3 rounded-xl h-48 overflow-y-scroll scrollbar-hide divide-y-[1px] divide-gray-200">
          {data
            ? (
                (data.users as User[])
                  .filter((user) => user.id !== authUser?.id)
                  .slice(0, 10) as User[]
              ).map((user, i) => (
                <UserPill
                  key={i}
                  user={user}
                  size={12}
                  containerStyles="border-t-[1px] border-gray-100 py-2 cursor-pointer"
                  handleClick={() => navigate(`/user/profile/${user.username}`)}
                />
              ))
            : ""}
        </div>
      </div>
    </div>
  );
};

export default RightSidebar;
