import React, { useState } from "react";
import { RiHome3Line, RiUser3Line } from "react-icons/ri";
import { BsBookmarkStar } from "react-icons/bs";
import { AiOutlineAudio } from "react-icons/ai";
import { SiAudioboom } from "react-icons/si";
import { useNavigate, Link } from "react-router-dom";
import { useAuth } from "../../lib/useAuth";
import { Button, UserPill, Logo } from "../shared";
import { User } from "../../common/global-types";
import { toast } from "react-hot-toast";

const LeftSidebar: React.FC = () => {
  const navigate = useNavigate();
  const { isAuthenticated, authUser, logout } = useAuth();
  const [isHoveringUserAccountPill, setIsHoveringUserAccountPill] =
    useState<boolean>(false);

  const sidebarButtons = [
    {
      title: "Home",
      icon: <RiHome3Line size={20} />,
      path: "/feed",
    },
    {
      title: "Profile",
      icon: <RiUser3Line size={20} />,
      path: `/user/profile/${authUser?.username}`,
    },
    {
      title: "Bookmarks",
      icon: <BsBookmarkStar size={20} />,
      path: null,
    },
    {
      title: "Create Post",
      icon: <AiOutlineAudio size={20} />,
      path: "/post/create",
    },
  ];

  return (
    <>
      <div className="hidden md:block h-screen  relative min-w-[18rem] bg-gradient-light z-10">
        <Logo otherStyles="my-6" />
        <div className="py-5 px-8">
          <ul className="space-y-5">
            {sidebarButtons.map((item, i) => (
              <li key={i}>
                {item.path ? (
                  <Link to={item.path}>
                    <button
                      name={item.title}
                      aria-label={item.title}
                      type="button"
                      className="bg-white px-4 py-3 w-full rounded-lg shadow-xl inline-flex items-center hover:border-indigo-500 hover:font-semibold hover:text-indigo-500 hover:border-2 space-x-3"
                    >
                      {item.icon}
                      <span className="text-[18px]">{item.title}</span>
                    </button>
                  </Link>
                ) : (
                  <button
                    name={item.title}
                    aria-label={item.title}
                    type="button"
                    className="bg-white px-4 py-3 w-full rounded-lg shadow-xl inline-flex items-center hover:border-indigo-500 hover:font-semibold hover:text-indigo-500 hover:border-2 space-x-3"
                    onClick={() => toast.success("Coming soon! 😉")}
                  >
                    <span>{item.icon}</span>
                    <span className="text-[18px]">{item.title}</span>
                  </button>
                )}
              </li>
            ))}
          </ul>
        </div>

        <div className="absolute bottom-4 py-3 px-8 w-full">
          {isAuthenticated() ? (
            <div>
              {isHoveringUserAccountPill && (
                <div className="bg-white mb-2 px-4 py-2 rounded-lg shadow-lg">
                  <ul className="divide-y-[1px] divide-gray-200 space-y-2">
                    <li className="cursor-pointer hover:text-indigo-500 hover:font-semibold">
                      <Link to={`/user/profile/${authUser?.username}`}>
                        Profile
                      </Link>
                    </li>
                    <li
                      className="cursor-pointer hover:text-indigo-500 hover:font-semibold"
                      onClick={logout}
                    >
                      Logout
                    </li>
                  </ul>
                </div>
              )}
              <UserPill
                user={authUser as User}
                size={10}
                handleMouseOver={() => setIsHoveringUserAccountPill(true)}
                containerStyles="bg-white cursor-pointer justify-evenly p-1 rounded-full border-[1px] border-gray-500 shadow-md"
                handleClick={() => {
                  setIsHoveringUserAccountPill(false);
                }}
              />
            </div>
          ) : (
            <Button
              name="login"
              label="Log In"
              otherStyles="p-3 w-full"
              handleClick={() => navigate("/login")}
            />
          )}
        </div>
      </div>

      <div className="w-14 h-screen bg-indigo-500 md:hidden">
        <div className="flex justify-center items-center my-3">
          <SiAudioboom size={20} />
        </div>
        <ul className="space-y-5 flex justify-center items-center flex-col text-white py-8">
          {sidebarButtons.map((item, i) => (
            <li key={i}>
              {item.path ? (
                <Link to={item.path}>
                  <button type="button" aria-label={item.title}>
                    {item.icon}
                  </button>
                </Link>
              ) : (
                item.icon
              )}
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default LeftSidebar;
