import React, { useState } from "react";
import { Post } from "../../common/global-types";
import { useBottomPlayerContext } from "../../context";
import { PostCard } from "../Post";

interface IFeedPosts {
  feed: Post[] | null;
}

const FeedPosts: React.FC<IFeedPosts> = ({ feed }) => {
  const { setPlayerStatus, setAudioSrc, playerStatus, setCurrentTrackMeta } =
    useBottomPlayerContext();

  const [currentPostPlayingIdx, setCurrentPostPlayingIdx] = useState<
    number | null
  >(null);

  // play a post's audio
  const handlePlayPost = (post: Post) => {
    // activate the player
    setPlayerStatus({
      isPlaying: true,
      isActive: true,
      isOpen: true,
    });
    // set the current track for the player
    setCurrentTrackMeta({
      title: post.title as string,
      topic: post.category as string,
      username: post.creator?.username,
    });
    // set the audio source for the player
    setAudioSrc(post.audioFileUrl as string);
    setCurrentPostPlayingIdx(Number(post.id));
  };

  // pause the current playing post
  const handlePausePost = () => {
    //  set the player's isPlaying state to false
    setPlayerStatus((prev) => ({ ...prev, isPlaying: false }));
  };

  if (feed === null) {
    return <div>Loading...</div>;
  }

  return (
    <div className="mb-12">
      {feed.map((post, id) => (
        <PostCard
          post={post}
          key={id}
          isPlaying={currentPostPlayingIdx == post.id && playerStatus.isPlaying}
          isPaused={
            currentPostPlayingIdx == post.id &&
            playerStatus.isActive &&
            !playerStatus.isPlaying
          }
          handlePlay={() => handlePlayPost(post)}
          handlePause={handlePausePost}
        />
      ))}
    </div>
  );
};

export default FeedPosts;
