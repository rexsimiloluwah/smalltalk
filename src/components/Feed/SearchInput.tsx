import React, { useState } from "react";
import { MdCancel } from "react-icons/md";
import { RiSearch2Line } from "react-icons/ri";
import { useSearchInputContext } from "../../context/SearchInputContext";
import { FormInput } from "../shared";

interface ISearchInputProps {
  handleSearch: (searchString: string) => void;
}

const SearchInput: React.FC<ISearchInputProps> = ({ handleSearch }) => {
  const { searchActive, setSearchActive } = useSearchInputContext();
  const [searchString, setSearchString] = useState<string>("");

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const handleChangeSearchInput = (e: React.ChangeEvent<any>) => {
    setSearchActive(true);
    const input = e.target.value.toLowerCase();
    handleSearch(input);
    setSearchString(input);
  };

  return (
    <div>
      <div className="relative">
        <FormInput
          type="text"
          name="search"
          id="search"
          placeholder="Search for post, people, or topic.."
          addOn={true}
          handleChange={handleChangeSearchInput}
          showLabel={false}
          addOnIcon={<RiSearch2Line size={18} />}
        />
        {searchActive && searchString !== "" && (
          <button
            onClick={() => setSearchActive(!searchActive)}
            type="button"
            className="flex absolute inset-y-0 right-0 items-center pr-3 cursor-pointer text-red-500"
          >
            <MdCancel size={24} />
          </button>
        )}
      </div>
    </div>
  );
};

export default SearchInput;
