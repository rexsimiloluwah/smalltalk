import React, { useState, useEffect } from "react";
import { Follow, Post, User } from "../../common/global-types";
import { PostCard } from "../Post";
import { DefaultProfileImage, Divider, UserPill } from "../shared";
import { useNavigate, Link } from "react-router-dom";
import { useAuth } from "../../lib/useAuth";
import { FiArrowLeft } from "react-icons/fi";
import { useMutation } from "@apollo/client";
import { FOLLOW_USER, UNFOLLOW_USER } from "../../graphql/queries/user";
import { useBottomPlayerContext } from "../../context";
import { Audio } from "react-loader-spinner";
import { Toaster } from "react-hot-toast";
import { Modal } from "../shared";

interface IProfileMainProps {
  user: User;
}

// Checks if userA is following userB
const isFollowing = (userA: User, userB: User): boolean => {
  //console.log(userA, userB);
  if (!userB.followers || userB.followers.length === 0 || !userA) {
    return false;
  }
  for (const follow of userB.followers as Follow[]) {
    if (follow.follower?.id === userA.id) {
      return true;
    }
  }
  return false;
};

const ProfileMain: React.FC<IProfileMainProps> = ({ user }) => {
  const { authUser } = useAuth();
  const navigate = useNavigate();
  const [userState, setUserState] = useState<User>(user);
  const [followersModalOpen, setFollowersModalOpen] = useState<boolean>(false);
  const [followingModalOpen, setFollowingModalOpen] = useState<boolean>(false);
  const [currentPostPlayingIdx, setCurrentPostPlayingIdx] = useState<
    number | null
  >(null);
  const { playerStatus, setPlayerStatus, setAudioSrc, setCurrentTrackMeta } =
    useBottomPlayerContext();
  const [followUser] = useMutation(FOLLOW_USER);
  const [unfollowUser] = useMutation(UNFOLLOW_USER);

  useEffect(() => {
    setUserState(user);
  }, [user]);

  // play a post's audio
  const handlePlayPost = (post: Post) => {
    // activate the player
    setPlayerStatus({
      isPlaying: true,
      isActive: true,
      isOpen: true,
    });
    // set the current track for the player
    setCurrentTrackMeta({
      title: post.title as string,
      topic: post.category as string,
      username: post.creator?.username,
    });
    // set the audio source for the player
    setAudioSrc(post.audioFileUrl as string);
    setCurrentPostPlayingIdx(Number(post.id));
  };

  // pause the current playing post
  const handlePausePost = () => {
    //  set the player's isPlaying state to false
    setPlayerStatus((prev) => ({ ...prev, isPlaying: false }));
  };

  /**
   * Follow a user
   * @param userId // accepts the ID of the user to be followed
   */
  const handleFollowUser = async (userId: number) => {
    await followUser({
      variables: {
        where: {
          id: userId,
        },
      },
    })
      .then(() => {
        setUserState({
          ...userState,
          followers: [
            ...(userState?.followers as Follow[]),
            { follower: { id: authUser?.id } } as Follow,
          ],
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  /**
   * Unfollow a user
   * @param userId // accepts the ID of the user to be unfollowed
   */
  const handleUnfollowUser = async (userId: number) => {
    await unfollowUser({
      variables: {
        where: {
          id: userId,
        },
      },
    })
      .then(() => {
        setUserState({
          ...userState,
          followers: userState?.followers?.filter(
            (follower) => follower.id !== authUser?.id
          ),
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // Returns a loader if the fetched user data is loading
  if (!userState) {
    return (
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50 flex items-center justify-center">
        <Audio color="fuchsia" height={40} />
      </div>
    );
  }

  return (
    <>
      <Toaster />
      {/* Modal to display list of user's followers */}
      <Modal
        title={`Followers (${user?.followers?.length})`}
        isOpen={followersModalOpen}
        handleClose={() => setFollowersModalOpen(!followersModalOpen)}
      >
        <div className="divide-y-[1px] divide-gray-200">
          {user?.followers?.map((follower, id) => (
            <div key={id} className="py-2">
              <UserPill user={follower.follower as User} size={20} />
            </div>
          ))}
        </div>
      </Modal>

      {/* Modal to display list of user's following */}
      <Modal
        title={`Following (${user?.following?.length})`}
        isOpen={followingModalOpen}
        handleClose={() => setFollowingModalOpen(!followingModalOpen)}
      >
        <div className="divide-y-[1px] divide-gray-200">
          {user?.following?.map((follower, id) => (
            <div key={id} className="py-2">
              <UserPill user={follower.user as User} size={20} />
            </div>
          ))}
        </div>
      </Modal>

      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50">
        <div className="border-b-[1px] py-4 px-8  sticky top-0 bg-white z-50">
          <button
            name="go_back"
            onClick={() => navigate(-1)}
            type="button"
            className="shadow-lg p-3 rounded-full bg-gradient-dark hover:opacity-100 group transition-all"
          >
            <FiArrowLeft
              size={24}
              className="text-white group-hover:-translate-x-1 duration-200 "
            />
          </button>
        </div>
        <div className="md:px-8 px-4 py-4 space-y-6">
          <div className="w-full bg-white relative rounded-xl border-[1px] border-indigo-400">
            <div className="h-20 bg-gradient-to-r from-indigo-200 via-red-200 to-yellow-100 w-full rounded-t-xl"></div>
            <div className="absolute right-4 my-2">
              {authUser?.id === user?.id ? (
                <Link to={`/user/update`}>
                  <button
                    name="update_profile"
                    className="border-[1px] border-gray-500 text-black p-2 rounded-lg hover:border-2 hover:border-indigo-500 hover:text-indigo-500 hover:font-semibold"
                  >
                    Update Profile
                  </button>
                </Link>
              ) : isFollowing(authUser as User, userState as User) ? (
                <div className="space-x-2">
                  <button
                    name="following"
                    className="border-[1px] border-gray-500 text-black p-2 rounded-lg pointer-events-none"
                  >
                    Following
                  </button>
                  <button
                    onClick={() => handleUnfollowUser(Number(user?.id))}
                    name="unfollow"
                    className="text-white bg-gradient-dark p-2 rounded-lg opacity-90 hover:opacity-100"
                  >
                    Unfollow
                  </button>
                </div>
              ) : (
                <button
                  onClick={() => handleFollowUser(Number(user?.id))}
                  name="follow"
                  className="p-2 rounded-lg text-white bg-gradient-dark opacity-90 hover:opacity-100"
                >
                  Follow
                </button>
              )}
            </div>
            <div className="mt-[-45px] md:mt-[-60px] mx-[30px]">
              <DefaultProfileImage
                firstname={user?.firstname as string}
                lastname={user?.lastname as string}
                otherStyles="border-2 border-white md:w-24 md:h-24 w-16 h-16"
              />
            </div>
            <div className="md:px-8 px-4 pt-6 md:py-1 space-y-3">
              <div>
                <h1 className="font-bold text-xl">
                  {user?.firstname} {user?.lastname}
                </h1>
                <p className="text-gray-00">@{user?.username}</p>
              </div>
              <p className="text-[1rem]">{user?.profile?.bio}</p>
              <div className="flex space-x-6">
                <div
                  className="text-gray-500 hover:text-indigo-600 cursor-pointer"
                  onClick={() => setFollowingModalOpen(true)}
                >
                  <span className="text-indigo-600">
                    {userState?.following?.length}
                  </span>{" "}
                  Following
                </div>

                <div
                  className="text-gray-500 hover:text-indigo-600 cursor-pointer"
                  onClick={() => setFollowersModalOpen(true)}
                >
                  <span className="text-indigo-600">
                    {userState?.followers?.length}
                  </span>{" "}
                  Followers
                </div>
              </div>
            </div>
          </div>
          <Divider />
          <div className="my-8">
            {(user?.posts as Post[]).map((post, id) => (
              <PostCard
                post={post}
                key={id}
                isPlaying={
                  currentPostPlayingIdx === Number(post.id) &&
                  playerStatus.isPlaying
                }
                isPaused={
                  currentPostPlayingIdx === Number(post.id) &&
                  playerStatus.isActive
                }
                handlePlay={() => handlePlayPost(post)}
                handlePause={handlePausePost}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileMain;
