import { useMutation } from "@apollo/client";
import React, { useState } from "react";
import toast from "react-hot-toast";
import { DELETE_USER } from "../../graphql/queries/user";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../lib/useAuth";

interface IDeleteUserDialogProps {
  handleClose: () => void;
}

const DeleteUserDialog: React.FC<IDeleteUserDialogProps> = ({
  handleClose,
}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [deleteUser] = useMutation(DELETE_USER);

  const {clearAuthUser} = useAuth()

  const navigate = useNavigate()

  const handleDeleteUser = async () => {
    setLoading(true);
    await deleteUser()
      .then((data) => {
        setLoading(false);
        toast.success("User deleted successfully, Sorry to lose you!");
        clearAuthUser()
        navigate("/")
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
        toast.error("User deletion failed: ", error);
      });
  };
  return (
    <div className="flex justify-center space-x-3 my-3">
      <button
        name="delete_user"
        type="button"
        onClick={handleDeleteUser}
        className="bg-red-500 p-2 opacity-95 hover:opacity-100 rounded-lg text-white"
      >
        <span>Yes, Close Account</span>{" "}
        {loading ? (
          <svg
            className="w-6 h-6 animate-spin mr-1"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
          </svg>
        ) : (
          ""
        )}
      </button>
      <button
        name="close"
        type="button"
        onClick={handleClose}
        className="bg-green-500 p-2 opacity-95 hover:opacity-100 rounded-lg text-white"
      >
        No, Just Kidding :)
      </button>
    </div>
  );
};

export default DeleteUserDialog;
