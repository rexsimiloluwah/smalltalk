import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, FormInput } from "../shared";
import { MdEditNote } from "react-icons/md";
import { BsFacebook, BsInstagram, BsPhone, BsTwitter } from "react-icons/bs";
import { UserProfile } from "../../common/global-types/user-profile";
import { useMutation } from "@apollo/client";
import { UPDATE_USER_PROFILE } from "../../graphql/queries/user";
import toast from "react-hot-toast";

const UserProfileUpdateForm: React.FC<{ profile: UserProfile }> = ({
  profile,
}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const { bio, twitterUrl, facebookUrl, instagramUrl, phoneNumber } = profile;
  const [updateUserProfile] = useMutation(UPDATE_USER_PROFILE);

  const formik = useFormik({
    initialValues: {
      bio: bio || "",
      twitterUrl: twitterUrl || "",
      facebookUrl: facebookUrl || "",
      instagramUrl: instagramUrl || "",
      phoneNumber: phoneNumber || "",
    },
    onSubmit: async (values) => {
      //console.log(bio, twitterUrl, facebookUrl, instagramUrl, phoneNumber);
      // Update the account settings here
      setLoading(true);
      await updateUserProfile({
        variables: {
          data: { ...values },
        },
      })
        .then((data) => {
          console.log(data);
          setLoading(false);
          toast.success("User Profile Updated Successfully 🎉.");
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
          toast.error("User Profile Update Failed: ", error);
        });
    },
    validationSchema: Yup.object({
      bio: Yup.string().label("Bio").notRequired(),
      twitterUrl: Yup.string().label("Twitter URL").url().notRequired(),
      facebookUrl: Yup.string().label("Facebook URL").url().notRequired(),
      instagramUrl: Yup.string().label("Instagram URL").url().notRequired(),
      phoneNumber: Yup.string().label("Phone Number").notRequired(),
    }),
  });
  return (
    <div>
      <form
        className="flex flex-col space-y-3 w-full justify-center"
        onSubmit={formik.handleSubmit}
      >
        <div>
          <div className="relative">
            <FormInput
              name="bio"
              id="bio"
              placeholder="Enter New Bio"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.bio}
              error={formik.errors.bio}
              addOn={true}
              value={formik.values.bio}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <MdEditNote />
            </div>
          </div>
          {formik.touched.bio && formik.errors.bio && (
            <span className="text-red-600 text-sm">{formik.errors.bio}</span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="twitterUrl"
              id="twitterUrl"
              placeholder="Enter New Twitter URL"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.twitterUrl}
              error={formik.errors.twitterUrl}
              addOn={true}
              value={formik.values.twitterUrl}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <BsTwitter />
            </div>
          </div>
          {formik.touched.twitterUrl && formik.errors.twitterUrl && (
            <span className="text-red-600 text-sm">
              {formik.errors.twitterUrl}
            </span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="instagramUrl"
              id="instagramUrl"
              placeholder="Enter New Instagram URL"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.instagramUrl}
              error={formik.errors.instagramUrl}
              addOn={true}
              value={formik.values.instagramUrl}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <BsInstagram />
            </div>
          </div>
          {formik.touched.instagramUrl && formik.errors.instagramUrl && (
            <span className="text-red-600 text-sm">
              {formik.errors.instagramUrl}
            </span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="facebookUrl"
              id="facebookUrl"
              placeholder="Enter New Facebook URL"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.facebookUrl}
              error={formik.errors.facebookUrl}
              addOn={true}
              value={formik.values.facebookUrl}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <BsFacebook />
            </div>
          </div>
          {formik.touched.facebookUrl && formik.errors.facebookUrl && (
            <span className="text-red-600 text-sm">
              {formik.errors.facebookUrl}
            </span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="phoneNumber"
              id="phoneNumber"
              placeholder="Enter New Phone Number"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.phoneNumber}
              error={formik.errors.phoneNumber}
              addOn={true}
              value={formik.values.phoneNumber}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <BsPhone />
            </div>
          </div>
          {formik.touched.phoneNumber && formik.errors.phoneNumber && (
            <span className="text-red-600 text-sm">
              {formik.errors.phoneNumber}
            </span>
          )}
        </div>

        <Button
          name="update_user_profile"
          type="submit"
          label="Update User Profile"
          loading={loading}
          loadingText="Updating User Profile..."
          otherStyles="p-3 w-fit"
          disabled={!(formik.dirty && formik.isValid)}
        />
      </form>
    </div>
  );
};

export default UserProfileUpdateForm;
