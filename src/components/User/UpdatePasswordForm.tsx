import { useFormik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import { Button, FormInput } from "../shared";
import { RiLockPasswordLine, RiEyeFill, RiEyeOffLine } from "react-icons/ri";
import { useMutation } from "@apollo/client";
import { UPDATE_USER_PASSWORD } from "../../graphql/queries/user";
import toast from "react-hot-toast";

const UpdatePasswordForm: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [updateUserPassword] = useMutation(UPDATE_USER_PASSWORD);
  const formik = useFormik({
    initialValues: {
      password: "",
    },
    onSubmit: async (values) => {
      const { password } = values;
      setLoading(true);
      await updateUserPassword({
        variables: {
          data: {
            password: password,
          },
        },
      })
        .then((data) => {
          console.log(data);
          setLoading(false);
          toast.success("User Password Updated Successfully 🎉");
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
          toast.error("User Password Updated Failed: ", error);
        });
    },
    validationSchema: Yup.object({
      password: Yup.string()
        .min(8, "Password must contain at least 8 characters.")
        .label("Password"),
    }),
  });
  return (
    <div>
      <form
        className="flex flex-col space-y-3 w-full justify-center"
        onSubmit={formik.handleSubmit}
      >
        <div>
          <div className="relative">
            <FormInput
              type={showPassword ? "text" : "password"}
              name="password"
              id="password"
              placeholder="Enter Password"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.password}
              error={formik.errors.password}
              addOn={true}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <RiLockPasswordLine />
            </div>
          </div>
          {formik.touched.password && formik.errors.password && (
            <span className="text-red-600 text-sm">
              {formik.errors.password}
            </span>
          )}
        </div>
        <Button
          name="update_password"
          type="submit"
          label="Update Password"
          loadingText="Updating Password..."
          otherStyles="p-3 w-fit"
          disabled={!(formik.dirty && formik.isValid)}
          loading={loading}
        />
      </form>
    </div>
  );
};

export default UpdatePasswordForm;
