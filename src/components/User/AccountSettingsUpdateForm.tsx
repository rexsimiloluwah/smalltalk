import React, { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, FormInput } from "../shared";
import { MdOutlineAlternateEmail } from "react-icons/md";
import { RiUser3Line } from "react-icons/ri";
import { User } from "../../common/global-types";
import { useMutation } from "@apollo/client";
import { UPDATE_USER } from "../../graphql/queries/user";
import toast from "react-hot-toast";

const AccountSettingsUpdateForm: React.FC<{ user: User }> = ({ user }) => {
  const { email, firstname, lastname, username } = user;
  const [loading, setLoading] = useState<boolean>(false);
  const [updateUserAccount] = useMutation(UPDATE_USER);

  const formik = useFormik({
    initialValues: {
      email: email || "",
      firstname: firstname || "",
      lastname: lastname || "",
      username: username || "",
    },
    onSubmit: async (values) => {
      // console.log(email, firstname, lastname, username);

      // Update the account settings here
      setLoading(true);
      await updateUserAccount({
        variables: {
          data: {
            ...values,
          },
        },
      })
        .then((data) => {
          console.log(data);
          setLoading(false);
          toast.success("User Updated Successfully 🎉.");
        })
        .catch((error) => {
          setLoading(false);
          toast.error("User Updated Failed!: ", error);
        });
    },
    validationSchema: Yup.object({
      email: Yup.string().email().label("Email"),
      firstname: Yup.string().min(2).label("Firstname"),
      lastname: Yup.string().min(2).label("Lastname"),
      username: Yup.string().min(2).label("Username"),
    }),
  });

  return (
    <div>
      <form
        className="flex flex-col space-y-3 w-full justify-center"
        onSubmit={formik.handleSubmit}
      >
        <div>
          <div className="relative">
            <FormInput
              name="email"
              id="email"
              placeholder="Enter New E-mail"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.email}
              error={formik.errors.email}
              value={formik.values.email}
              addOn={true}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <MdOutlineAlternateEmail />
            </div>
          </div>

          {formik.touched.email && formik.errors.email && (
            <span className="text-red-600 text-sm">{formik.errors.email}</span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="firstname"
              id="firstname"
              placeholder="Enter New Firstname"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.firstname}
              error={formik.errors.firstname}
              value={formik.values.firstname}
              addOn={true}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <RiUser3Line />
            </div>
          </div>
          {formik.touched.firstname && formik.errors.firstname && (
            <span className="text-red-600 text-sm">
              {formik.errors.firstname}
            </span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="lastname"
              id="lastname"
              placeholder="Enter New Lastname"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.lastname}
              error={formik.errors.lastname}
              value={formik.values.lastname}
              addOn={true}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <RiUser3Line />
            </div>
          </div>
          {formik.touched.lastname && formik.errors.lastname && (
            <span className="text-red-600 text-sm">
              {formik.errors.lastname}
            </span>
          )}
        </div>

        <div>
          <div className="relative">
            <FormInput
              name="username"
              id="username"
              placeholder="Enter Username"
              handleChange={formik.handleChange}
              handleBlur={formik.handleBlur}
              touched={formik.touched.username}
              error={formik.errors.username}
              value={formik.values.username}
              addOn={true}
            />
            <div className="flex absolute inset-y-0 left-0 top-6 items-center pl-3 pointer-events-none text-gray-400">
              <RiUser3Line />
            </div>
          </div>
          {formik.touched.username && formik.errors.username && (
            <span className="text-red-600 text-sm">
              {formik.errors.username}
            </span>
          )}
        </div>

        <Button
          name="update_account"
          type="submit"
          label="Update Account"
          loadingText="Updating Account..."
          otherStyles="p-3 w-fit"
          disabled={!(formik.dirty && formik.isValid)}
          loading={loading}
        />
      </form>
    </div>
  );
};

export default AccountSettingsUpdateForm;
