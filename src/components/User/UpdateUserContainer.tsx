import React, { useState } from "react";
import { Toaster } from "react-hot-toast";
import { BsTrash } from "react-icons/bs";
import { User } from "../../common/global-types";
import { UserProfile } from "../../common/global-types/user-profile";
import { useAuth } from "../../lib/useAuth";
import { Accordion, Modal } from "../shared";
import NavBack from "../shared/NavBack";
import AccountSettingsUpdateForm from "./AccountSettingsUpdateForm";
import DeleteUserDialog from "./DeleteUserDialog";
import UpdatePasswordForm from "./UpdatePasswordForm";
import UserProfileUpdateForm from "./UserProfileUpdateForm";

const UpdateUserContainer: React.FC = () => {
  const { authUser } = useAuth();
  const updateUserAccordions = [
    {
      title: "Update User Account",
      description: "Update User Settings",
      form: <AccountSettingsUpdateForm user={authUser as User} />,
    },
    {
      title: "Update User Profile",
      description: "Update User Profile",
      form: (
        <UserProfileUpdateForm profile={authUser?.profile as UserProfile} />
      ),
    },
    {
      title: "Update Password",
      description: "Update User Password",
      form: <UpdatePasswordForm />,
    },
  ];

  const [activeAccordionIdx, setActiveAccordionIdx] = useState<number | null>(
    null
  );
  const [showCloseAccountModal, setShowCloseAccountModal] =
    useState<boolean>(false);
  return (
    <>
      <Toaster />
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50">
        <Modal
          isOpen={showCloseAccountModal}
          handleClose={() => setShowCloseAccountModal(!showCloseAccountModal)}
          title="Confirm Account Closure"
        >
          <DeleteUserDialog
            handleClose={() => setShowCloseAccountModal(!showCloseAccountModal)}
          />
        </Modal>
        <NavBack />
        <div className="w-[90%] md:w-[70%] py-4 m-auto space-y-5">
          {updateUserAccordions.map((item, id) => (
            <Accordion
              title={item.title}
              description={item.description}
              isActive={
                activeAccordionIdx === id && activeAccordionIdx !== null
              }
              key={id}
              handleClick={() => {
                if (activeAccordionIdx === id) {
                  setActiveAccordionIdx(null);
                  return;
                }
                setActiveAccordionIdx(id);
              }}
            >
              {item.form}
            </Accordion>
          ))}
          <button
            name="update_account"
            type="button"
            onClick={() => setShowCloseAccountModal(true)}
            className="p-3 bg-red-600 opacity-90 hover:opacity-100 text-white rounded-lg flex justify-between items-center space-x-2"
          >
            <BsTrash /> <span>Close Account</span>
          </button>
        </div>
      </div>
    </>
  );
};

export default UpdateUserContainer;
