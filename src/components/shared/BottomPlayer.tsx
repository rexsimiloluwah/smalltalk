import React, { useEffect, useRef } from "react";
import { useBottomPlayerContext } from "../../context";
import { Audio as AudioSpinner } from "react-loader-spinner";
import { RiPlayFill } from "react-icons/ri";
import { MdCancel, MdPause } from "react-icons/md";

const BottomPlayer: React.FC = () => {
  const {
    clearPlayer,
    audioSrc,
    playerStatus,
    setPlayerStatus,
    setTrackDuration,
    setTrackProgress,
    trackDuration,
    trackProgress,
    currentTrackMeta,
  } = useBottomPlayerContext();
  const audioRef = useRef<HTMLAudioElement | null>(null);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const intervalRef = useRef<any>();

  // set the audio source when it is changed
  useEffect(() => {
    if (audioRef.current) {
      audioRef.current.pause();
      audioRef.current.currentTime = 0;
    }
    audioRef.current = new Audio(audioSrc);
  }, [audioSrc]);

  /**
   * Start a timer for monitoring the track progress
   */
  const startTimer = () => {
    // set the current interval tracker
    // tick every 1s
    intervalRef.current = setInterval(() => {
      if (audioRef?.current?.ended) {
        setPlayerStatus((prev) => ({ ...prev, isPlaying: false }));
        clearPlayer();
      } else {
        setTrackProgress(audioRef.current?.currentTime as number);
      }
    }, 1000);
  };

  /**
   * Format an audio duration (in ms) to a human readable format
   * @param duration // accepts the duration in ms
   * @returns // returns a human readable representation of the duration i.e. mm:ss
   */
  const formatAudioDuration = (duration: number): string => {
    // format duration: secs -> min:sec
    let mins = 0;
    while (duration >= 60) {
      mins += 1;
      duration = duration - 60;
    }
    //format
    return [
      mins.toString().padStart(2, "0"),
      Math.round(duration).toString().padStart(2, "0"),
    ].join(":");
  };

  // Handle the playing state for a track
  useEffect(() => {
    if (playerStatus.isPlaying && audioRef.current !== null) {
      audioRef?.current?.play();
      setTrackDuration(audioRef.current.duration);
      startTimer();
    } else {
      audioRef?.current?.pause();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [playerStatus.isPlaying, audioRef.current?.duration]);

  return (
    <div
      className={`${
        playerStatus.isOpen ? "fixed" : "hidden"
      } border-t-2 md:border-none bottom-0 w-full px-8 py-4 text-white bg-indigo-500 z-[1000] flex justify-between flex-wrap shadow-lg`}
    >
      <MdCancel
        className="absolute top-1 right-1 cursor-pointer"
        size={20}
        onClick={() => setPlayerStatus({ ...playerStatus, isOpen: false })} //close the player
      />
      <div className="flex space-x-3 items-center">
        {playerStatus.isPlaying ? (
          <div className="space-x-3 flex items-center">
            <AudioSpinner
              height="20"
              width="20"
              color="#fff"
              ariaLabel="audio-loading"
              wrapperStyle={{}}
              wrapperClass="wrapper-class"
              visible={true}
            />
            <button
              aria-label="pause_track"
              onClick={
                () => setPlayerStatus({ ...playerStatus, isPlaying: false }) // pause the current track
              }
            >
              <MdPause size={24} />
            </button>
          </div>
        ) : (
          <button
            aria-label="play_track"
            onClick={() => {
              setPlayerStatus({ ...playerStatus, isPlaying: true }); // for playing the current track
            }}
          >
            <RiPlayFill size={24} className="cursor-pointer" />
          </button>
        )}
        <span>LISTENING TO: {currentTrackMeta?.title.substring(0, 80)}...</span>
      </div>
      <span className="font-semibold">
        {formatAudioDuration(trackProgress)}/
        {trackDuration ? formatAudioDuration(trackDuration) : "--:--"}
      </span>
    </div>
  );
};

export default BottomPlayer;
