import React, { useState } from "react";
import Button from "./Button";
import { useNavigate, Link } from "react-router-dom";
import { useAuth } from "../../lib/useAuth";
import { Toaster } from "react-hot-toast";
import Logo from "./Logo";
import Modal from "./Modal";

const Navbar: React.FC = () => {
  const navigate = useNavigate();
  const { isAuthenticated, logout } = useAuth();
  const [showMobileNavModal, setShowMobileNavModal] = useState<boolean>(false);

  const authNavLinks = [
    { title: "About", path: null, section: "about_section" },
    { title: "Your Feed", path: "/feed", section: "" },
  ];

  const guestNavLinks = [
    { title: "About", path: null, section: "about_section" },
    { title: "Log In", path: "/login", section: "" },
  ];

  // scroll to a particular section on the page
  const handleScrollToSection = (section: string) => {
    // get the location of the section
    const location = document.getElementById(section)?.offsetTop;
    const navbarHeight = document.getElementById("navbar")?.offsetHeight;
    // scroll to the location
    window.scrollTo({
      left: 0,
      top: Number(location) - Number(navbarHeight) - 32, // offset by height of the navbar
    });
  };

  return (
    <>
      <Toaster />
      <Modal
        isOpen={showMobileNavModal}
        handleClose={() => setShowMobileNavModal(!showMobileNavModal)}
        title="SmallTalk"
      >
        {isAuthenticated() ? (
          <ul className="flex flex-col space-y-4 items-center py-2 text-lg">
            {authNavLinks.map((item, i) => (
              <li
                className="hover:font-semibold cursor-pointer hover:text-indigo-500"
                key={i}
              >
                {item.path ? (
                  <Link to={item.path}>{item.title}</Link>
                ) : (
                  <span
                    onClick={() => {
                      setShowMobileNavModal(false);
                      handleScrollToSection(item.section);
                    }}
                  >
                    {item.title}
                  </span>
                )}
              </li>
            ))}
            <li>
              <Button
                name="logout"
                label="Logout"
                type="button"
                otherStyles="px-4 py-2"
                handleClick={logout}
              />
            </li>
          </ul>
        ) : (
          <ul className="flex flex-col space-y-4 items-center py-2 text-lg">
            {guestNavLinks.map((item, i) => (
              <li
                className="hover:font-semibold cursor-pointer hover:text-indigo-500"
                key={i}
              >
                {item.path ? (
                  <Link to={item.path}>{item.title}</Link>
                ) : (
                  <span
                    onClick={() => {
                      setShowMobileNavModal(false);
                      handleScrollToSection(item.section);
                    }}
                  >
                    {item.title}
                  </span>
                )}
              </li>
            ))}
            <li>
              <Button
                name="join_smalltalk"
                label="Join SmallTalk"
                type="button"
                otherStyles="px-4 py-2"
                handleClick={() => navigate("/signup")}
              />
            </li>
          </ul>
        )}
      </Modal>
      <div
        id="navbar"
        className="flex sticky top-0 justify-between items-center w-full md:px-12 px-4 py-6 border-b-2 border-gray-300 font-uchen text-lg z-[999] bg-white"
      >
        <Logo />
        <div className="md:hidden block mobile__hamburger">
          <Button
            name="menu"
            label="Menu"
            otherStyles="px-2 py-1"
            handleClick={() => setShowMobileNavModal(!showMobileNavModal)}
          />
        </div>
        <div className="hidden md:block">
          {isAuthenticated() ? (
            <ul className="flex justify-between md:space-x-6 space-x-4 items-center text-lg">
              {authNavLinks.map((item, i) => (
                <li
                  className="hover:font-semibold cursor-pointer hover:text-indigo-500"
                  key={i}
                >
                  {item.path ? (
                    <Link to={item.path}>{item.title}</Link>
                  ) : (
                    <span onClick={() => handleScrollToSection(item.section)}>
                      {item.title}
                    </span>
                  )}
                </li>
              ))}
              <li>
                <Button
                  name="logout"
                  label="Logout"
                  type="button"
                  otherStyles="px-4 py-2"
                  handleClick={logout}
                />
              </li>
            </ul>
          ) : (
            <ul className="flex justify-between md:space-x-6 space-x-4 items-center">
              {guestNavLinks.map((item, i) => (
                <li
                  className="hover:font-semibold cursor-pointer hover:text-indigo-500"
                  key={i}
                >
                  {item.path ? (
                    <Link to={item.path}>{item.title}</Link>
                  ) : (
                    <span onClick={() => handleScrollToSection(item.section)}>
                      {item.title}
                    </span>
                  )}
                </li>
              ))}
              <li>
                <Button
                  name="join_smalltalk"
                  label="Join SmallTalk"
                  type="button"
                  otherStyles="px-4 py-2"
                  handleClick={() => navigate("/signup")}
                />
              </li>
            </ul>
          )}
        </div>
      </div>
    </>
  );
};

export default Navbar;
