import React from "react";

const Divider: React.FC = () => {
  return <hr className="border-r-[0.1px] border-gray-400 opacity-25 my-2" />;
};

export default Divider;
