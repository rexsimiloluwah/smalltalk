import React from "react";

interface IFormRowProps {
  children?: React.ReactNode;
}

const FormRow: React.FC<IFormRowProps> = ({ children }) => {
  return (
    <div className="w-full grid grid-cols-1 md:grid-cols-2 gap-3">
      {children}
    </div>
  );
};

export default FormRow;
