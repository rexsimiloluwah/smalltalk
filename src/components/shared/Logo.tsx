import React from "react";
import { SiAudioboom } from "react-icons/si";
import { Link } from "react-router-dom";

const Logo: React.FC<{ otherStyles?: string }> = ({ otherStyles }) => {
  return (
    <Link
      to="/"
      className={`${otherStyles} font-semibold text-xl flex space-x-2 items-center justify-center`}
    >
      <SiAudioboom size={24} />
      <span>Small Talk</span>
    </Link>
  );
};

export default Logo;
