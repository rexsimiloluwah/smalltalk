import React from "react";

interface ICategorySelectInputProps {
  name: string;
  id?: string;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlur: (e: React.ChangeEvent<HTMLInputElement>) => void;
  defaultCategories?: string[];
}

const CategorySelectInput: React.FC<ICategorySelectInputProps> = ({
  handleChange,
  handleBlur,
  name,
  id,
  defaultCategories,
}) => {
  const categories = defaultCategories || [
    "Health",
    "Programming",
    "Anime",
    "Music",
    "JavaScript",
    "Depression",
    "News",
    "Food",
    "Nigeria",
    "Afrobeats",
    "Economy",
    "Finance",
  ];
  return (
    <div>
      <input
        name={name}
        id={id}
        type="text"
        list="categories"
        className="p-3 rounded-lg border-2 border-indigo-500 w-full"
        placeholder="Type or Select Topic"
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <datalist id="categories">
        {categories.map((item, i) => (
          <option value={item} key={i}>
            {item}
          </option>
        ))}
      </datalist>
    </div>
  );
};

export default CategorySelectInput;
