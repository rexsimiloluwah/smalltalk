import { useNavigate } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import React from "react";

const NavBack: React.FC = () => {
  const navigate = useNavigate();
  return (
    <div className="border-b-[1px] py-4 px-8 sticky top-0 bg-white z-50">
      <button
        name="go_back"
        onClick={() => navigate(-1)}
        type="button"
        className="shadow-lg p-3 rounded-full bg-gradient-dark hover:opacity-100 group transition-all"
      >
        <FiArrowLeft
          size={24}
          className="text-white group-hover:-translate-x-1 duration-200 "
        />
      </button>
    </div>
  );
};

export default NavBack;
