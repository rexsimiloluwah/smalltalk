import React, { useState } from "react";

interface ITagsInputProps {
  tags: string[];
  setTags: React.Dispatch<React.SetStateAction<string[]>>;
}

const TagsInput: React.FC<ITagsInputProps> = ({ tags, setTags }) => {
  const [tagInput, setTagInput] = useState<string>("");
  const [isKeyReleased, setIsKeyReleased] = useState<boolean>(false);

  const deleteTag = (i: number) => {
    setTags((prev) => prev.filter((_, index) => index !== i));
  };
  return (
    <div className="flex w-full max-w-full bg-white p-2 overflow-scroll scrollbar-hide">
      {tags.map((tag, i) => (
        <div
          className="flex bg-gradient-to-r from-rose-400 via-fuchsia-500 to-indigo-500 mx-2 whitespace-nowrap p-1 rounded-lg"
          key={i}
        >
          <span className="text-white">{tag}</span>
          <button
            name="remove_tag"
            type="button"
            className="ml-2 font-mono font-semibold"
            onClick={() => deleteTag(i)}
          >
            x
          </button>
        </div>
      ))}
      <input
        type="text"
        name="tag"
        id="tag"
        placeholder="#tag1,#tag2"
        className="border-none w-full min-w-[50%] caret-current"
        value={tagInput}
        onChange={(e) => {
          setTagInput(e.target.value);
        }}
        onKeyUp={() => {
          setIsKeyReleased(true);
        }}
        onKeyDown={(e) => {
          const { key } = e;
          const currentTag = tagInput.trim().slice(1);
          if (
            tagInput.charAt(0) === "#" &&
            (key === "," || key === "Enter") &&
            currentTag.length &&
            !tags.includes(currentTag)
          ) {
            e.preventDefault();
            // append the current tag to the tags state
            setTags((prev) => [...prev, currentTag]);
            // clear the current tag
            setTagInput("");
          }

          // for deleting tags
          if (
            key === "Backspace" &&
            tags.length > 0 &&
            !tagInput.length &&
            isKeyReleased
          ) {
            e.preventDefault();
            const tagsCopy = [...tags];
            const poppedTag = tagsCopy.pop();
            setTags(tagsCopy);
            setTagInput(poppedTag as string);
          }

          setIsKeyReleased(false);
        }}
      />
    </div>
  );
};

export default TagsInput;
