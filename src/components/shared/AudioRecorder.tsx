/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-nocheck
import React, { useEffect, useState } from "react";
import { MdOutlineMic, MdStopCircle, MdCancel } from "react-icons/md";
import { Audio } from "react-loader-spinner";

interface IAudioRecorderProps {
  handleRecordingBlob: (blob: Blob) => void;
}

// interface Navigator {
//   getUserMedia(
//     options: { video?: boolean; audio?: boolean },
//     success: (stream: any) => void,
//     error?: (error: string) => void
//   ): void;
// }

const AudioRecorder: React.FC<IAudioRecorderProps> = ({
  handleRecordingBlob,
}) => {
  const MAX_RECORDING_DURATION = 5 * 60 * 1000; // 5 minutes
  const [recorderState, setRecordingState] = useState({
    isRecording: false,
    isBlocked: false,
    blobURL: null,
  });
  const [recorder, setRecorder] = useState<MediaRecorder | null>(null);
  const [timeMs, setTimeMs] = useState<number>(0);
  const [finalAudioBlob, setFinalAudioBlob] = useState<Blob | null>(null);

  // For handling recording timer
  useEffect(() => {
    if (recorderState.isRecording) {
      const timer = setTimeout(() => {
        setTimeMs((prev) => prev + 1000);
      }, 1000);

      if (timeMs > MAX_RECORDING_DURATION) {
        stopRecording();
      }

      return () => clearTimeout(timer);
    }
  });

  // For initializing the microphone access
  useEffect(() => {
    navigator.getUserMedia(
      {
        audio: {
          sampleRate: 44100,
          channelCount: 2,
        },
      },
      () => {
        console.log("Permission Granted");
        setRecordingState((prev) => ({ ...prev, isBlocked: false }));
      },
      () => {
        console.log("Permission Denied");
        setRecordingState((prev) => ({ ...prev, isBlocked: true }));
      }
    );
  }, []);

  // Start the recording
  const startRecording = async () => {
    const constraints = { audio: true };
    const recordingChunks: BlobPart[] = [];
    const onSuccess = (stream: MediaStream) => {
      // initialize the recorder
      const newRecorder = new MediaStreamRecorder(stream);
      newRecorder.start(1000);
      setRecorder(newRecorder);
      setRecordingState((prev) => ({ ...prev, isRecording: true }));

      newRecorder.ondataavailable = (e: BlobEvent) => {
        recordingChunks.push(e.data);
      };

      newRecorder.onstop = () => {
        const blob = new Blob(recordingChunks as BlobPart[], {
          type: "audio/ogg; codecs=opus",
        });
        const audioBlobURL = window.URL.createObjectURL(blob);
        setRecordingState((prev) => ({
          ...prev,
          isRecording: false,
          blobURL: audioBlobURL,
        }));
        setTimeMs(0);
        setFinalAudioBlob(blob);
        handleRecordingBlob(blob);
        // clear the recording chunks
        const reader = new FileReader();
        reader.readAsDataURL(blob);
        reader.onloadend = () => {
          console.log(reader.result);
        };
      };
    };

    const onFailure = () => {
      alert("Browser does not support media recording!");
    };

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onFailure);
  };

  // Stop the recording
  const stopRecording = async () => {
    recorder?.stop();
  };

  // Cancel current recording
  const cancelRecording = () => {
    setFinalAudioBlob(null);
    handleRecordingBlob(null);
    setRecordingState((prev) => ({
      ...prev,
      blobURL: "",
    }));
  };

  // Format time duration (number => mm:ss)
  const formatDuration = (durationMs: number): string => {
    // convert duration to seconds
    let duration = durationMs / 1000;
    let mins = 0;
    while (duration >= 60) {
      mins += 1;
      duration = duration - 60;
    }
    //format
    return [mins, Math.round(duration).toString().padStart(2, "0")].join(":");
  };

  return (
    <div className="space-y-3">
      <div className="flex flex-wrap space-x-4">
        <button
          type="button"
          className="flex space-x-3 bg-indigo-500 items-center h-12 p-3 shadow-lg"
          onClick={startRecording}
        >
          {recorderState.isRecording ? (
            <span>
              <Audio
                height="20"
                width="20"
                color="#000"
                ariaLabel="audio-loading"
                wrapperStyle={{}}
                wrapperClass="wrapper-class"
                visible={true}
              />
            </span>
          ) : (
            <MdOutlineMic size={24} />
          )}
          {recorderState.isRecording ? (
            <div className="flex space-x-2">
              <span>Recording...</span>
              <span className="font-semibold">
                {formatDuration(timeMs)}/05:00
              </span>
            </div>
          ) : (
            <span>Start Recording</span>
          )}
        </button>
        {recorderState.isRecording && (
          <button
            type="button"
            className="flex space-x-3 bg-red-500 items-center h-12 p-3 shadow-lg"
            onClick={stopRecording}
          >
            <span>Stop Recording</span>
            <MdStopCircle />
          </button>
        )}
      </div>
      {finalAudioBlob !== null && (
        <div className="flex justify-between bg-white rounded-lg p-2 items-center">
          <audio controls="controls" className="bg-transparent">
            <source src={recorderState.blobURL} />
          </audio>
          <button
            type="button"
            className="text-red-500"
            onClick={cancelRecording}
          >
            <MdCancel size={32} />
          </button>
        </div>
      )}
    </div>
  );
};

export default AudioRecorder;
