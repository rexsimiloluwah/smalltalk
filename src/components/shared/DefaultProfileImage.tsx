import React from "react";

const DefaultProfileImage: React.FC<{
  firstname: string;
  lastname: string;
  otherStyles?: string;
}> = ({ firstname, lastname, otherStyles }) => {
  const label =
    firstname.charAt(0).toUpperCase() + lastname.charAt(0).toUpperCase();
  return (
    <div
      className={`${otherStyles} bg-gradient-dark flex justify-center items-center font-semibold text-white rounded-full`}
    >
      {label}
    </div>
  );
};

export default DefaultProfileImage;
