import React from "react";
import { User } from "../../common/global-types";
import DefaultProfileImage from "./DefaultProfileImage";

interface IUserPillProps {
  user: User;
  size: number;
  containerStyles?: string;
  handleClick?: () => void;
  handleMouseOver?: () => void;
  handleMouseOut?: () => void;
}

const UserPill: React.FC<IUserPillProps> = ({
  user,
  size,
  containerStyles,
  handleClick,
  handleMouseOver,
  handleMouseOut,
}) => {
  return (
    <div
      className={`flex space-x-2 ${containerStyles}`}
      onClick={handleClick}
      onMouseEnter={handleMouseOver}
      onMouseOut={handleMouseOut}
    >
      {user?.profileImageUrl ? (
        <img
          src={user.profileImageUrl}
          alt=""
          className={`w-${size} h-${size} rounded-full`}
        />
      ) : (
        <DefaultProfileImage
          firstname={user?.firstname as string}
          lastname={user?.lastname as string}
          otherStyles="w-12 h-12"
        />
      )}

      <div className="flex-1 overflow-hidden flex-grow">
        <h1 className="truncate font-semibold">
          {user?.firstname} {user?.lastname}
        </h1>
        <p className="text-gray-600 flex-grow">@{user?.username}</p>
      </div>
    </div>
  );
};

export default UserPill;
