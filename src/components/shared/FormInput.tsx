import React, { useState } from "react";
import { RiEyeFill, RiEyeOffLine } from "react-icons/ri";

interface IFormInputProps {
  type?: string;
  name: string;
  id?: string;
  touched?: boolean;
  error?: string;
  placeholder: string;
  handleChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlur?: (e: React.FocusEvent<HTMLInputElement>) => void;
  autocomplete?: string;
  value?: string;
  addOn?: boolean;
  addOnIcon?: React.ReactNode;
  showLabel?: boolean;
  otherStyles?: string;
}

const FormInput: React.FC<IFormInputProps> = ({
  type,
  name,
  id,
  touched,
  error,
  placeholder,
  handleChange,
  handleBlur,
  autocomplete,
  value,
  addOn,
  addOnIcon,
  otherStyles,
  showLabel,
}) => {
  const [showPassword, setShowPassword] = useState<boolean>(false);

  return (
    <div>
      {showLabel && (
        <label htmlFor={name}>
          {name.charAt(0).toUpperCase() + name.slice(1)}
          <span className="text-red-500 font-semibold">*</span>
        </label>
      )}
      <div>
        <div className="relative">
          <input
            type={showPassword ? "text" : type}
            name={name}
            id={id}
            className={`p-3 rounded-lg ${
              touched && error ? "border-red-600" : "border-indigo-600"
            } border-2 w-full ${addOn && `pl-8`} ${otherStyles}`}
            placeholder={placeholder}
            onChange={handleChange}
            onBlur={handleBlur}
            autoComplete="new-password"
            value={value}
            role="presentation"
          />
          <div className="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none text-gray-400">
            {addOnIcon}
          </div>
          {type === "password" && (
            <div
              className="flex absolute inset-y-0 right-0 items-center pr-3 cursor-pointer"
              onClick={() => setShowPassword(!showPassword)}
            >
              {showPassword ? (
                <RiEyeFill size={18} />
              ) : (
                <RiEyeOffLine size={18} />
              )}
            </div>
          )}
        </div>
        {touched && error && (
          <span className="text-red-600 text-sm">{error}</span>
        )}
      </div>
    </div>
  );
};

FormInput.defaultProps = {
  type: "text",
  showLabel: true,
};
export default FormInput;
