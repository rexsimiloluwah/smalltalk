import React from "react";

interface IContainerProps {
  children?: React.ReactNode;
  otherStyles?: string;
  id?: string;
}

const Container: React.FC<IContainerProps> = ({
  children,
  otherStyles,
  id,
}) => {
  return (
    <div
      className={`md:w-[80%] w-[95%] m-auto font-uchen py-8 ${otherStyles}`}
      id={id}
    >
      {children}
    </div>
  );
};

export default Container;
