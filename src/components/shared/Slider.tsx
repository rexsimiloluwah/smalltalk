import React, { useRef } from "react";
import { BiChevronRight, BiChevronLeft } from "react-icons/bi";

const Slider: React.FC<{
  sliderItems: React.ReactNode[];
  noOfItems?: number;
  autoSlide?: boolean;
  slideInterval?: number;
}> = ({ noOfItems, sliderItems }) => {
  const sliderRef = useRef<HTMLDivElement | null>(null);
  // const [scrollDirection, setScrollDirection] = useState<"l" | "r">("r");
  const slideLeft = () => {
    if (sliderRef.current) {
      sliderRef.current.scrollLeft =
        (sliderRef.current?.scrollLeft as number) - 200;
      console.log(sliderRef.current.scrollLeft);
    }
  };

  const slideRight = () => {
    if (sliderRef.current) {
      sliderRef.current.scrollLeft =
        (sliderRef.current?.scrollLeft as number) + 200;
    }
  };

  //   useEffect(() => {
  //     if (
  //       Number(sliderRef.current?.scrollLeft) >
  //       Number(sliderRef.current?.scrollWidth) / 2
  //     ) {
  //       setScrollDirection("l");
  //     }
  //     if (Number(sliderRef.current?.scrollLeft) == 0) {
  //       setScrollDirection("r");
  //     }
  //   }, [sliderRef]);

  //   useEffect(() => {
  //     const sliderInterval = setInterval(() => {
  //       if (scrollDirection == "l") {
  //         slideLeft();
  //       }
  //       if (scrollDirection == "r") {
  //         slideRight();
  //       }
  //     }, 1000);

  //     return () => clearInterval(sliderInterval);
  //   }, []);
  const minCardW = Math.round(100 / Number(noOfItems)) - 4;
  const cardStyle: React.CSSProperties = {
    minWidth: `${minCardW}%`,
  };
  return (
    <div className="w-full bg-gray-50 relative flex items-center">
      <button
        type="button"
        className={`slider__icon left bg-indigo-400 h-10 w-10 rounded-full flex justify-center items-center absolute left-0 opacity-80 hover:opacity-100 z-10 text-white`}
        onClick={slideLeft}
      >
        <BiChevronLeft size={34} />
      </button>
      <div
        className="main__slider w-full h-full inline-flex overflow-x-scroll flex-nowrap space-x-2 p-2 scroll-smooth scrollbar-hide"
        ref={sliderRef}
      >
        {sliderItems.map((item, i) => (
          <div style={cardStyle} key={i}>
            {item}
          </div>
        ))}
      </div>
      <button
        type="button"
        className="slider__icon right bg-indigo-400 h-10 w-10 rounded-full flex justify-center items-center absolute right-0 opacity-80 hover:opacity-100 cursor-pointer z-10 text-white"
        onClick={slideRight}
      >
        <BiChevronRight size={32} />
      </button>
    </div>
  );
};

Slider.defaultProps = {
  noOfItems: 3,
};

export default Slider;
