import React from "react";
import { BiMinus } from "react-icons/bi";
import { BsPlus } from "react-icons/bs";

interface IAccordionProps {
  title: string;
  description?: string;
  children: React.ReactNode;
  isActive: boolean;
  handleClick: () => void;
}
const Accordion: React.FC<IAccordionProps> = ({
  title,
  children,
  isActive,
  handleClick,
}) => {
  return (
    <div className="accordion">
      <div className="accordion__item bg-white border-2 border-indigo-600 p-2">
        <div className="accordion__title flex justify-between items-center">
          <span>{title}</span>
          <button
            name="open_close_accordion"
            type="button"
            aria-label="Open"
            onClick={() => {
              handleClick();
            }}
          >
            {isActive ? (
              <BiMinus size={32} strokeWidth={1} className="text-indigo-600" />
            ) : (
              <BsPlus size={32} strokeWidth={1} className="text-indigo-600" />
            )}
          </button>
        </div>
      </div>
      {isActive && (
        <div className="accordion__content bg-gray-50 py-3 md:px-8 px-3 border-[1px] border-fuchsia-200 m-auto">
          {children}
        </div>
      )}
    </div>
  );
};

export default Accordion;
