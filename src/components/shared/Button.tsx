import React from "react";
import { Link } from "react-router-dom";

interface IButtonProps {
  type?: "button" | "submit" | "reset" | undefined;
  handleClick?: () => void;
  label: string;
  otherStyles?: string;
  loading?: boolean;
  loadingText?: string;
  disabled?: boolean;
  icon?: React.ReactNode;
  href?: string;
  name?: string;
}

const Button: React.FC<IButtonProps> = ({
  name,
  label,
  type,
  handleClick,
  otherStyles,
  loading,
  loadingText,
  disabled,
  icon,
  href,
}) => {
  if (href) {
    return (
      <Link to={href}>
        <button
          name={name}
          type={type}
          disabled={disabled}
          aria-label={label}
          className={`${otherStyles} bg-gradient-dark  hover:shadow-md text-white rounded-xl hover:-translate-y-0.5 transition ease-in-out flex space-x-2 items-center justify-center ${
            disabled
              ? `opacity-50 cursor-not-allowed hover:opacity-50`
              : `opacity-95 hover:opacity-100 cursor-pointer`
          }`}
        >
          {loading ? (
            <span className="flex items-center justify-center">
              <svg
                className="w-6 h-6 animate-spin mr-1"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
              </svg>
              {loadingText}
            </span>
          ) : (
            <>
              <span>{label}</span>
              {icon}
            </>
          )}
        </button>
      </Link>
    );
  }
  return (
    <button
      name={name}
      type={type}
      disabled={disabled}
      aria-label={label}
      onClick={handleClick}
      className={`${otherStyles} bg-gradient-dark  hover:shadow-md text-white rounded-xl hover:-translate-y-0.5 transition ease-in-out flex space-x-2 items-center justify-center ${
        disabled
          ? `opacity-50 cursor-not-allowed hover:opacity-50`
          : `opacity-95 hover:opacity-100 cursor-pointer`
      }`}
    >
      {loading ? (
        <span className="flex items-center justify-center">
          <svg
            className="w-6 h-6 animate-spin mr-1"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z" />
          </svg>
          {loadingText}
        </span>
      ) : (
        <>
          <span>{label}</span>
          {icon}
        </>
      )}
    </button>
  );
};

export default Button;
