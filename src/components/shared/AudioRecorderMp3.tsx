// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
// TODO: - for some reason, the lamejs encoder does not work as desired in production
// How can we solve this problem?
import React, { useState, useEffect } from "react";
import * as MicRecorder from "mic-recorder-to-mp3";
import { MdOutlineMic, MdStopCircle, MdCancel } from "react-icons/md";
import { Audio } from "react-loader-spinner";

interface IAudioRecorderProps {
  handleRecordingBlob: (blob: Blob) => void;
}

const AudioRecorderMp3: React.FC<IAudioRecorderProps> = ({
  handleRecordingBlob,
}) => {
  const MAX_RECORDING_DURATION = 5 * 60 * 1000; // 5 minutes
  const [recorderState, setRecordingState] = useState({
    isRecording: false,
    isBlocked: false,
    blobURL: null,
  });

  const [Mp3Recorder, setMp3Recorder] = useState(null);
  const [timeMs, setTimeMs] = useState<number>(0);
  const [finalAudioBlob, setFinalAudioBlob] = useState<Blob | null>(null);

  useEffect(() => {
    if (recorderState.isRecording) {
      const timer = setTimeout(() => {
        setTimeMs((prev) => prev + 1000);
      }, 1000);

      if (timeMs > MAX_RECORDING_DURATION) {
        stopRecording();
      }

      return () => clearTimeout(timer);
    }
  });

  useEffect(() => {
    navigator.getUserMedia(
      { audio: true },
      () => {
        console.log("Permission Granted");
        setRecordingState((prev) => ({ ...prev, isBlocked: false }));
        setMp3Recorder(
          new MicRecorder({
            bitRate: 128,
          })
        );
      },
      () => {
        console.log("Permission Denied");
        setRecordingState((prev) => ({ ...prev, isBlocked: true }));
      }
    );
  }, []);

  // Start recording
  const startRecording = () => {
    // Start recording. Browser will request permission to use your microphone.
    if (recorderState.isBlocked) {
      alert("Microphone access not granted.");
      return;
    }
    console.log("Starting recording...");
    console.log(Mp3Recorder);
    Mp3Recorder.start()
      .then((response) => {
        setRecordingState((prev) => ({ ...prev, isRecording: true }));
        console.log(response);
      })
      .catch((e) => {
        console.error(e);
      });
  };

  // Stop the recording
  // Clear the timer, and set the final recorded audio blob
  const stopRecording = () => {
    Mp3Recorder.stop()
      .getMp3()
      .then(([buffer, blob]) => {
        setTimeMs(0);
        console.log(buffer);
        console.log(blob);
        const blobURL = URL.createObjectURL(blob);
        setRecordingState((prev) => ({
          ...prev,
          isRecording: false,
          blobURL,
        }));
        setFinalAudioBlob(blob);
        handleRecordingBlob(blob);
      })
      .catch((e) => {
        alert("We could not retrieve your message");
        console.log(e);
      });
  };

  // Cancel current recording
  const cancelRecording = () => {
    setFinalAudioBlob(null);
    handleRecordingBlob(null);
    setRecordingState((prev) => ({
      ...prev,
      blobURL: "",
    }));
  };

  // Format time duration (number => mm:ss)
  const formatDuration = (durationMs: number): string => {
    // convert duration to seconds
    let duration = durationMs / 1000;
    let mins = 0;
    while (duration >= 60) {
      mins += 1;
      duration = duration - 60;
    }
    //format
    return [mins, Math.round(duration).toString().padStart(2, "0")].join(":");
  };

  return (
    <div className="space-y-3">
      <div className="flex flex-wrap space-x-4">
        <button
          name="start_recording"
          aria-label="start_recording"
          type="button"
          className="flex space-x-3 bg-indigo-600 items-center h-12 p-3 shadow-lg text-white"
          onClick={startRecording}
        >
          {recorderState.isRecording ? (
            <span>
              <Audio
                height="20"
                width="20"
                color="#000"
                ariaLabel="audio-loading"
                wrapperStyle={{}}
                wrapperClass="wrapper-class"
                visible={true}
              />
            </span>
          ) : (
            <MdOutlineMic size={24} />
          )}
          {recorderState.isRecording ? (
            <div className="flex space-x-2">
              <span>Recording...</span>
              <span className="font-semibold">
                {formatDuration(timeMs)}/05:00
              </span>
            </div>
          ) : (
            <span>Start Recording</span>
          )}
        </button>
        {recorderState.isRecording && (
          <button
            name="stop_recording"
            aria-label="stop_recording"
            type="button"
            className="flex space-x-3 bg-red-500 items-center h-12 p-3 shadow-lg"
            onClick={stopRecording}
          >
            <span>Stop Recording</span>
            <MdStopCircle />
          </button>
        )}
      </div>
      {finalAudioBlob !== null && (
        <div className="flex justify-between bg-white rounded-lg p-2 items-center">
          <audio
            src={recorderState.blobURL}
            controls="controls"
            className="bg-transparent"
          />
          <button
            type="button"
            className="text-red-500"
            onClick={cancelRecording}
          >
            <MdCancel size={32} />
          </button>
        </div>
      )}
    </div>
  );
};

export default AudioRecorderMp3;
