import React from "react";
import ReactDOM from "react-dom";
import { MdCancel } from "react-icons/md";

interface IModalProps {
  isOpen: boolean;
  handleClose: () => void;
  title: string;
  children?: React.ReactNode;
}

const Modal = ({ isOpen, handleClose, title, children }: IModalProps) => {
  const modalContent = isOpen ? (
    <div className="modal fixed top-0 left-0 right-0 bottom-0 z-50 font-uchen">
      <div
        className="modal__backdrop fixed blur-[1px] top-0 bottom-0 right-0 left-0 z-[1] bg-[rgba(0,0,0,0.7)]"
        onClick={handleClose}
      ></div>
      <div className="modal__dialog grid h-screen place-items-center">
        <div className="bg-white w-[85%] md:w-[36%] rounded-xl z-[2] relative m-auto">
          <button
            name="close_modal"
            aria-label="close_modal"
            type="button"
            onClick={handleClose}
            className="absolute right-3 top-3 text-red-500 opacity-95 hover:opacity-100"
          >
            <MdCancel size={26} />
          </button>
          <div className="modal__main p-4">
            <div className="modal__header text-xl text-indigo-500 font-semibold text-center">
              {title}
            </div>
            <div className="modal__body">{children}</div>
          </div>
        </div>
      </div>
    </div>
  ) : null;

  return ReactDOM.createPortal(
    modalContent,
    document.getElementById("modal-hook") as Element
  );
};

export default Modal;
