import React, { useState } from "react";
import AudioRecorder from "../shared/AudioRecorderMp3";
import { useAuth } from "../../lib/useAuth";
import {
  Button,
  FormInput,
  TagsInput,
  CategorySelectInput,
} from "../../components/shared";
import { CREATE_POST_MUTATION } from "../../graphql/queries/post";
import { useFormik } from "formik";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { SiAudioboom } from "react-icons/si";

const CreatePost: React.FC = () => {
  const navigate = useNavigate();
  const [tags, setTags] = useState<string[]>([]);
  const [audioBlob, setAudioBlob] = useState<Blob | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const { createPostApolloClient } = useAuth();
  const client = createPostApolloClient();

  const handleRecordingBlob = (blob: Blob) => {
    setAudioBlob(blob);
  };

  const formik = useFormik({
    initialValues: {
      title: "",
      topic: "",
    },
    onSubmit: async (values) => {
      const { title, topic } = values;
      const data = {
        title,
        category: topic,
        tags: tags.join(","),
      };
      setLoading(true);
      client
        .mutate({
          mutation: CREATE_POST_MUTATION,
          variables: { data: data, file: audioBlob },
        })
        .then(() => {
          setLoading(false);
          toast.success("Post created successfully 🎉..");
          navigate("/feed");
        })
        .catch((error) => {
          setLoading(false);
          toast.error(`Post creation failed: ${error}`);
        });
    },
    validationSchema: Yup.object({
      title: Yup.string().required().label("Title").min(2).max(150),
      topic: Yup.string().required().label("Topic"),
    }),
  });

  return (
    <>
      <Toaster />
      <div className="m-auto min-h-screen flex items-center justify-center flex-col font-uchen">
        <div className="w-[90%] md:w-[45%] shadow-lg bg-gradient-to-r from-indigo-200 via-red-200 to-yellow-100 py-5 px-8 rounded-xl flex flex-col justify-center">
          <h1 className="text-center text-xl md:text-3xl font-bold my-2">
            Create Post
          </h1>
          <form className="space-y-3" onSubmit={formik.handleSubmit}>
            <div className="relative">
              <div className="absolute right-0">
                  <span
                    className={`${
                      formik.values.title.length > 150 && `text-red-600`
                    }`}
                  >
                    {formik.values.title.length}
                  </span>
                  /150
                </div>
              <FormInput
                name="title"
                id="title"
                placeholder="Enter Post Title i.e. Explaining Naira De-valuation in Nigeria"
                touched={formik.touched.title}
                error={formik.errors.title}
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur}
              />
            </div>

            <div>
              <label htmlFor="topic">Topic</label>
              <CategorySelectInput
                name="topic"
                id="topic"
                handleChange={formik.handleChange}
                handleBlur={formik.handleBlur}
              />
              {formik.errors.topic && formik.touched.topic && (
                <span className="text-red-600 text-sm">
                  {formik.errors.topic}
                </span>
              )}
            </div>

            <TagsInput tags={tags} setTags={setTags} />

            <AudioRecorder handleRecordingBlob={handleRecordingBlob} />

            <Button
              label="Create Post"
              loadingText="Creating Post..."
              type="submit"
              loading={loading}
              otherStyles="p-3"
              disabled={!(formik.dirty && formik.isValid) || audioBlob === null}
              icon={<SiAudioboom />}
            />
          </form>
        </div>
      </div>
    </>
  );
};

export default CreatePost;
