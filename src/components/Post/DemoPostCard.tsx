import React, { useState } from "react";
import { Post } from "../../common/global-types";
import { DefaultProfileImage } from "../shared";
import {
  RiPauseLine,
  RiPlayFill,
  RiHeart2Line,
  RiMessage2Line,
} from "react-icons/ri";
import { BsThreeDots } from "react-icons/bs";
import PostCardMenu from "./PostCardMenu";
import { useBottomPlayerContext } from "../../context";

interface IPostCardProps {
  isPlaying?: boolean;
  isPaused?: boolean;
  post: Post;
  handlePlay?: () => void;
  handlePause?: () => void;
}

const PostCard: React.FC<IPostCardProps> = ({
  isPlaying,
  isPaused,
  post,
  handlePlay,
  handlePause,
}) => {
  const { title, creator, likes } = post;
  // For controlling the post card menu state
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const { trackProgress, trackDuration } = useBottomPlayerContext();
  const progressBarStyle = `bg-gradient-dark h-[0.4rem] rounded-tl-3xl transition-all duration-300 opacity-80`;

  return (
    <div className="bg-white my-4 rounded-xl border-[1px] border-gray-500 relative cursor-pointer overflow-hidden">
      {(isPlaying || isPaused) && (
        <div
          className={progressBarStyle}
          style={{
            width:
              `${Math.max(
                2,
                isNaN(trackProgress / trackDuration)
                  ? 0
                  : Math.round((trackProgress / trackDuration) * 100)
              )}%` || `0%`,
          }}
        ></div>
      )}
      <div
        className="pt-10 pb-4 md:px-10 px-3 space-y-4"
        onMouseEnter={() => setIsMenuOpen(false)}
      >
        <div className="flex space-x-2">
          <DefaultProfileImage
            firstname={creator?.firstname as string}
            lastname={creator?.lastname as string}
            otherStyles="w-10 h-10"
          />

          <div>
            <h1>
              <span className="hover:underline font-bold">
                {creator?.firstname} {creator?.lastname}
              </span>
            </h1>
            <span className="text-gray-400 hover:underline">
              @{creator?.username}
            </span>
          </div>
        </div>

        <div>
          <p>{title}</p>
        </div>
      </div>
      {/* Play Button */}
      <button
        type="button"
        name="play"
        onClick={isPlaying ? handlePause : handlePlay}
        className={`play__button ${
          isPlaying ? "opacity-100" : "opacity-50"
        } hover:opacity-100 text-white p-2 rounded-full absolute top-2 right-4 bg-gradient-dark`}
      >
        {isPlaying ? <RiPauseLine size={32} /> : <RiPlayFill size={32} />}
      </button>
      {/* Post Menu Button */}
      <div className={`p-2 rounded-full absolute top-1 left-2`}>
        <button
          type="button"
          name="post-menu"
          className="hover:opacity-100 focus:opacity-100 opacity-60 text-indigo-600"
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        >
          <BsThreeDots size={32} />
        </button>
        {isMenuOpen && <PostCardMenu post={post} />}
      </div>

      {/* Bottom */}
      <div className="border-t-[1px] border-gray-500 py-2 md:px-10 px-2 flex space-x-4">
        <div className="flex items-center space-x-2 text-gray-500 hover:text-indigo-500 cursor-pointer">
          <RiHeart2Line size={24} />
          <span>{likes?.length}</span>
        </div>
        <div className="flex items-center space-x-2 text-gray-500 hover:text-indigo-500 cursor-pointer">
          <RiMessage2Line size={24} />
          <span>{post?.comments?.length}</span>
        </div>

        <span className="text-gray-500">1.35k Listens</span>
      </div>
    </div>
  );
};

export default PostCard;
