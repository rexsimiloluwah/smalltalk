import React from "react";
import { Comment, Post } from "../../common/global-types";
import NavBack from "../shared/NavBack";
import PostCard from "./PostCard";
import PostComments from "./PostComments";
import { useBottomPlayerContext } from "../../context";
import { Audio } from "react-loader-spinner";

interface IPostViewProps {
  post: Post | null;
}

const PostView: React.FC<IPostViewProps> = ({ post }) => {
  const { setPlayerStatus, setAudioSrc, playerStatus, setCurrentTrackMeta } =
    useBottomPlayerContext();

  const handlePlayPost = (post: Post) => {
    setPlayerStatus({
      isPlaying: true,
      isActive: true,
      isOpen: true,
    });
    setCurrentTrackMeta({
      title: post.title as string,
      topic: post.category as string,
      username: post.creator?.username,
    });
    setAudioSrc(post.audioFileUrl as string);
  };

  const handlePausePost = () => {
    setPlayerStatus((prev) => ({ ...prev, isPlaying: false }));
  };

  // return a loader if the fetched post is in a loading state
  if (!post) {
    return (
      <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50 flex items-center justify-center">
        <Audio color="fuchsia" height={40} />
      </div>
    );
  }
  return (
    <div className="flex-grow relative overflow-y-scroll h-screen bg-fuchsia-50">
      <NavBack />
      <div className="md:px-8 px-4 py-4 mb-12">
        <PostCard
          post={post}
          isPlaying={playerStatus.isPlaying}
          handlePlay={() => handlePlayPost(post)}
          handlePause={handlePausePost}
        />
        <PostComments
          comments={post.comments as Comment[]}
          postId={Number(post.id)}
        />
      </div>
    </div>
  );
};

export default PostView;
