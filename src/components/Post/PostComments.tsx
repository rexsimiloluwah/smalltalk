import React, { useRef, useState } from "react";
import { useMutation } from "@apollo/client";
import { CREATE_POST_COMMENT } from "../../graphql/queries/comment";
import { toast } from "react-hot-toast";
import PostComment from "./PostComment";
import { Button } from "../shared";
import { Comment } from "../../common/global-types";
import { useAuth } from "../../lib/useAuth";

const PostComments: React.FC<{ comments: Comment[]; postId: number }> = ({
  comments,
  postId,
}) => {
  const [commentText, setCommentText] = useState<string>("");
  const [commentsState, setCommentsState] = useState<Comment[]>(comments)
  const {authUser} = useAuth()

  const commentBoxRef = useRef<HTMLTextAreaElement|null>(null)

  const [createComment, { loading }] = useMutation(CREATE_POST_COMMENT);

  const clearCommentBox = () => {
    if(commentBoxRef.current){
      commentBoxRef.current.value = ""
    }
  }

  const handleCreateComment = async () => {
    try {
      const {data: {createComment: newComment}} = await createComment({
        variables: { data: { content: commentText, postId: postId } },
      });
      toast.success(`Comment added successfully 🎉`);

      setTimeout(() => {
        setCommentsState([{...newComment, creator: authUser} as Comment, ...commentsState])
        clearCommentBox()
      }, 1000)
    } catch (error) {
      toast.error(`Could not create comment: ${error}`);
    }
  };

  return (
    <div className="bg-white my-4 rounded-xl border-[1px] border-indigo-100 p-4">
      <div className="pb-3">
        <textarea
          name="comment"
          id="comment"
          className="w-full border-2 border-gray-400 rounded-xl p-1 hover:border-indigo-500"
          rows={3}
          placeholder="Enter Comment..."
          ref={commentBoxRef}
          onChange={(e) => setCommentText(e.target.value)}
        ></textarea>
        <Button
          label="Comment"
          otherStyles="p-3"
          disabled={commentText.length === 0}
          handleClick={handleCreateComment}
          loading={loading}
          loadingText="Creating comment..."
        />
      </div>
      <div className="comments py-3">
        {commentsState.length > 0 ? (
          <div>
            {commentsState.map((comment, id) => (
              <PostComment comment={comment} key={id} />
            ))}
          </div>
        ) : (
          <div className="flex justify-center text-xl my-6">
            No Comments, Yet!
          </div>
        )}
      </div>
    </div>
  );
};

export default PostComments;
