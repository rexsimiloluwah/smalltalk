import React, { useMemo, useState } from "react";
import { Like, Post } from "../../common/global-types";
import { DefaultProfileImage } from "../shared";
import {
  RiPauseLine,
  RiPlayFill,
  RiHeart2Line,
  RiMessage2Line,
} from "react-icons/ri";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { BsThreeDots } from "react-icons/bs";
import { useAuth } from "../../lib/useAuth";
import PostCardMenu from "./PostCardMenu";
import { useMutation } from "@apollo/client";
import { LIKE_POST, UNLIKE_POST } from "../../graphql/queries/post";
import toast from "react-hot-toast";
import { useBottomPlayerContext } from "../../context";

interface IPostCardProps {
  isPlaying?: boolean;
  isPaused?: boolean;
  post: Post;
  handlePlay: (post: Post) => void;
  handlePause: () => void;
}

const PostCard: React.FC<IPostCardProps> = ({
  isPlaying,
  isPaused,
  post,
  handlePlay,
  handlePause,
}) => {
  const { id, title, creator, likes, category } = post;
  const navigate = useNavigate();
  const { authUser } = useAuth();
  // For controlling the post card menu state
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const [postLikes, setPostLikes] = useState<Like[]>(likes as Like[]);
  const { trackProgress, trackDuration } = useBottomPlayerContext();

  // computes if a post has been liked by the auth user
  const isPostLikedByUser = useMemo(() => {
    return postLikes
      .map((like) => Number(like.user?.id))
      .includes(Number(authUser?.id));
  }, [postLikes, authUser, post]);

  // Like post mutation
  const [likePost] = useMutation(LIKE_POST);

  // Unlike post mutation
  const [unlikePost] = useMutation(UNLIKE_POST);

  /**
   * This function handles the liking or unliking of a post
   * It unlikes the post if it has been liked by the user, and likes otherwise
   * @returns
   */
  const handleLikeOrUnlikePost = async () => {
    if (!authUser) {
      return;
    }
    // check if the post has been liked by the user
    if (!isPostLikedByUser) {
      // like the post if false
      await likePost({ variables: { where: { id: Number(post.id) } } })
        .then(({ data }) => {
          const likedPost = data.likePost;
          setPostLikes((prev) => [...prev, likedPost]);
        })
        .catch((error) => {
          toast.error(`Could not like post!: ${error}`);
        });
      return;
    }
    // unlike the post if it has been liked
    await unlikePost({
      variables: { where: { id: Number(post.id) } },
    })
      .then(({ data }) => {
        const unlikedPost = data.unlikePost;
        setPostLikes((prev) =>
          prev.filter((post) => post.id !== unlikedPost.id)
        );
      })
      .catch((error) => {
        toast.error(`Could not like post!: ${error}`);
      });
  };

  // styling for the progress bar at the top of the post card
  const progressBarStyle = `bg-gradient-dark h-[0.4rem] rounded-tl-3xl transition-all duration-300 opacity-80`;

  return (
    <div className="bg-white my-4 rounded-xl border-[1px] border-gray-500 relative cursor-pointer overflow-hidden">
      {(isPlaying || isPaused) && (
        <div
          className={progressBarStyle}
          style={{
            width:
              `${Math.max(
                2,
                isNaN(trackProgress / trackDuration)
                  ? 0
                  : Math.round((trackProgress / trackDuration) * 100)
              )}%` || `0%`,
          }}
        ></div>
      )}
      <div
        className="pt-10 pb-4 md:px-10 px-3 space-y-4 relative"
        onMouseEnter={() => setIsMenuOpen(false)}
      >
        <div className="flex space-x-2 my-2">
          {creator?.profileImageUrl ? (
            <img
              src={creator.profileImageUrl}
              alt="user__photo"
              className="w-10 h-10 rounded-full"
            />
          ) : (
            <DefaultProfileImage
              firstname={creator?.firstname as string}
              lastname={creator?.lastname as string}
              otherStyles="w-10 h-10"
            />
          )}

          <div>
            <h1>
              <Link to={`/user/profile/${creator?.username}`}>
                <span className="hover:underline font-bold">
                  {creator?.firstname} {creator?.lastname}
                </span>
              </Link>
            </h1>
            <Link to={`/user/profile/${creator?.username}`}>
              <span className="text-gray-400 hover:underline">
                @{creator?.username}
              </span>
            </Link>
          </div>
        </div>

        <div onClick={() => navigate(`/post/${id}`)}>
          <p>{title}</p>
        </div>
      </div>
      {/* Play Button */}
      <button
        type="button"
        name="play"
        onClick={isPlaying ? handlePause : () => handlePlay(post)}
        className={`play__button ${
          isPlaying ? "opacity-100" : "opacity-50"
        } hover:opacity-100 text-white p-2 rounded-full absolute top-2 right-4 bg-gradient-dark`}
      >
        {isPlaying ? <RiPauseLine size={32} /> : <RiPlayFill size={32} />}
      </button>
      {/* Post Menu Button */}
      <div className={`p-2 rounded-full absolute top-1 left-2`}>
        <button
          type="button"
          name="post-menu"
          className="hover:opacity-100 focus:opacity-100 opacity-60 text-indigo-600"
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        >
          <BsThreeDots size={32} />
        </button>
        {isMenuOpen && <PostCardMenu post={post} />}
      </div>
      <div className="block absolute top-1 left-14 my-2 bg-fuchsia-300 px-2 rounded-full">
        {category}
      </div>

      {/* Bottom */}
      <div className="border-t-[1px] border-gray-500 py-2 md:px-10 px-2 flex space-x-4">
        <div className="flex items-center space-x-2 text-gray-500 hover:text-indigo-500 cursor-pointer">
          <RiHeart2Line
            size={24}
            className={`${isPostLikedByUser && "text-red-500"}`}
            onClick={handleLikeOrUnlikePost}
          />
          <span>{postLikes?.length}</span>
        </div>
        <Link to={`/post/${id}`}>
          <div className="flex items-center space-x-2 text-gray-500 hover:text-indigo-500 cursor-pointer">
            <RiMessage2Line size={24} />
            <span>{post?.comments?.length}</span>
          </div>
        </Link>

        <span className="text-gray-500">-- Listens</span>
      </div>
    </div>
  );
};

export default PostCard;
