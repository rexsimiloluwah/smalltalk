import React from "react";

import { Comment } from "../../common/global-types";
import { DefaultProfileImage } from "../shared";
import {AiOutlineDelete} from "react-icons/ai"
import { useAuth } from "../../lib/useAuth";
import { useMutation } from "@apollo/client";
import { DELETE_POST_COMMENT } from "../../graphql/queries/comment";
import toast from "react-hot-toast";

const PostComment: React.FC<{ comment: Comment }> = ({ comment }) => {
  const { creator, content } = comment;
  const {authUser} = useAuth()

  const [deleteComment, {loading}] = useMutation(DELETE_POST_COMMENT)

  const handleDeleteComment = async () => {
    const confirmation = confirm("Are you sure you want to delete this comment?")

    if(!confirmation) return

    try{
      await deleteComment({
        variables: {where: {id: Number(comment.id)}}
      })
      
      toast.success('Comment deleted successfully')
    }catch(error){
      console.log(error)
      toast.error(`Cound not delete comment: ${error}`)
    }
  }

  return (
    <div className="space-y-4 py-2 border-t-[1px] border-b-[1px] border-gray-100">
      <div className="flex space-x-2 relative">
        {creator?.profileImageUrl ? (
          <img
            src={creator.profileImageUrl}
            alt="user__photo"
            className="w-10 h-10 rounded-full"
          />
        ) : (
          <DefaultProfileImage
            firstname={creator?.firstname as string}
            lastname={creator?.lastname as string}
            otherStyles="w-10 h-10"
          />
        )}

        <div>
          <h1>
            {creator?.firstname} {creator?.lastname}
          </h1>
          <span className="text-gray-400">@{creator?.username}</span>
        </div>

        {authUser?.id === comment.creator.id ? 
        <button onClick={handleDeleteComment} className="absolute right-2 hover:text-red-500 hover:bg-gray-50 p-2 rounded-full">
          <AiOutlineDelete />
        </button> : null}
      </div>

      <div>
        <p>{content}</p>
      </div>
    </div>
  );
};

export default PostComment;
