import React from "react";
import { useAuth } from "../../lib/useAuth";
import { RiPencilLine, RiShareBoxLine, RiDownload2Line } from "react-icons/ri";
import { FiTrash } from "react-icons/fi";
import { Post } from "../../common/global-types";
import { useMutation } from "@apollo/client";
import { DELETE_POST_MUTATION } from "../../graphql/queries/post";
import toast from "react-hot-toast";

interface IPostCardMenuProps {
  post: Post;
}

const PostCardMenu: React.FC<IPostCardMenuProps> = ({ post }) => {
  const { authUser } = useAuth();
  const { creator, audioFileUrl } = post;

  const [deletePost] = useMutation(DELETE_POST_MUTATION);

  const handleDeletePost = async () => {
    const confirmation = confirm("Are you sure you want to delete this post?")

    if(!confirmation) return 
    
    await deletePost({ variables: { where: { id: Number(post.id) } } })
      .then(() => {
        toast.success("Successfully deleted post!");
      })
      .catch((error) => {
        toast.error(`Could not delete post!: ${error}`);
      });
  };

  return (
    <div className="bg-white shadow-xl p-4 rounded-lg text-gray-600 border-[1px] border-gray-100">
      <ul>
        <li
          className={`hover:text-indigo-500 hover:font-semibold flex items-center space-x-2 ${
            authUser && authUser?.id === creator?.id ? "" : "hidden"
          }`}
        >
          <RiPencilLine /> <span>Edit Post</span>
        </li>
        <li
          className={`hover:text-indigo-500 hover:font-semibold  flex items-center space-x-2 ${
            authUser && authUser?.id === creator?.id ? "" : "hidden"
          }`}
          onClick={handleDeletePost}
        >
          <FiTrash /> <span>Delete Post</span>
        </li>
        <li className="hover:text-indigo-500 hover:font-semibold flex items-center space-x-2">
          <RiShareBoxLine /> <span>Share Post</span>
        </li>
        <li className="hover:text-indigo-500 hover:font-semibold">
          <a href={audioFileUrl} className="flex items-center space-x-2">
            <RiDownload2Line /> <span>Download Audio</span>
          </a>
        </li>
      </ul>
    </div>
  );
};

export default PostCardMenu;
