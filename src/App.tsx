import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ProtectedRoute from "./ProtectedRoute";
import GuestRoute from "./GuestRoute";
import {
  Home,
  Login,
  SignUp,
  Feed,
  CreatePost,
  UserProfile,
  PostDetail,
  UpdateAccount,
} from "./pages";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <GuestRoute>
                <Home />
              </GuestRoute>
            }
          />
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<SignUp />} />
          <Route
            path="/feed"
            element={<ProtectedRoute>{<Feed />}</ProtectedRoute>}
          />
          <Route
            path="/post/create"
            element={<ProtectedRoute>{<CreatePost />}</ProtectedRoute>}
          />
          <Route
            path="/user/profile/:username"
            element={<ProtectedRoute>{<UserProfile />}</ProtectedRoute>}
          />
          <Route
            path="/post/:id"
            element={<ProtectedRoute>{<PostDetail />}</ProtectedRoute>}
          />
          <Route
            path="/user/update"
            element={<ProtectedRoute>{<UpdateAccount />}</ProtectedRoute>}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
