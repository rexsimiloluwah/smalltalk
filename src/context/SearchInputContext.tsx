import React, { Dispatch, createContext, useContext, useState } from "react";

interface ISearchInputContext {
  searchActive: boolean;
  setSearchActive: Dispatch<React.SetStateAction<boolean>>;
}

interface IContextProviderProps {
  children?: React.ReactNode;
}

const SearchInputContext = createContext({} as ISearchInputContext);

export const SearchInputProvider: React.FC<IContextProviderProps> = ({
  children,
}) => {
  const [searchActive, setSearchActive] = useState<boolean>(false);

  return (
    <SearchInputContext.Provider
      value={{
        searchActive,
        setSearchActive,
      }}
    >
      {children}
    </SearchInputContext.Provider>
  );
};

export const useSearchInputContext = () => useContext(SearchInputContext);
