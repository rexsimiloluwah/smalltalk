import React, { Dispatch, createContext, useContext, useState } from "react";

export type CurrentTrackMeta = {
  title: string;
  topic?: string;
  username?: string;
};

// isActive && isPlaying => track playing/listening
// isActive && !isPlaying => track paused
export type PlayerStatus = {
  isActive: boolean;
  isPlaying: boolean;
  isOpen: boolean;
};

interface IAppContext {
  audioSrc: string;
  setAudioSrc: Dispatch<React.SetStateAction<string>>;
  playerStatus: PlayerStatus;
  setPlayerStatus: Dispatch<React.SetStateAction<PlayerStatus>>;
  trackDuration: number;
  setTrackDuration: Dispatch<React.SetStateAction<number>>;
  trackProgress: number;
  setTrackProgress: Dispatch<React.SetStateAction<number>>;
  clearPlayer: () => void;
  currentTrackMeta: CurrentTrackMeta | null;
  setCurrentTrackMeta: Dispatch<React.SetStateAction<CurrentTrackMeta | null>>;
}

interface IContextProviderProps {
  children?: React.ReactNode;
}

const BottomPlayerContext = createContext({} as IAppContext);

export const BottomPlayerProvider: React.FC<IContextProviderProps> = ({
  children,
}) => {
  const [audioSrc, setAudioSrc] = useState<string>("");
  const [playerStatus, setPlayerStatus] = useState<PlayerStatus>({
    isActive: false,
    isPlaying: false,
    isOpen: false,
  });
  const [trackDuration, setTrackDuration] = useState<number>(0);
  const [trackProgress, setTrackProgress] = useState<number>(0);
  const [currentTrackMeta, setCurrentTrackMeta] = useState<{
    title: string;
    topic?: string;
    username?: string;
  } | null>(null);

  const clearPlayer = () => {
    setTrackDuration(0);
    setTrackProgress(0);
    setPlayerStatus({
      isActive: false,
      isPlaying: false,
      isOpen: false,
    });
  };

  return (
    <BottomPlayerContext.Provider
      value={{
        audioSrc,
        setAudioSrc,
        playerStatus,
        setPlayerStatus,
        trackDuration,
        setTrackDuration,
        trackProgress,
        setTrackProgress,
        clearPlayer,
        currentTrackMeta,
        setCurrentTrackMeta,
      }}
    >
      {children}
    </BottomPlayerContext.Provider>
  );
};

export const useBottomPlayerContext = () => useContext(BottomPlayerContext);
