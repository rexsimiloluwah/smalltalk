import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";

export const initApolloClient = (headers: object) => {
  const link = new HttpLink({
    uri: process.env.REACT_BACKEND_URL as string,
    headers: headers,
  });

  const client = new ApolloClient({
    link,
    cache: new InMemoryCache(),
  });
  return client;
};
