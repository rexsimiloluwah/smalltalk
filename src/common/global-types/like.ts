import { Post } from "./post";
import { User } from "./user";

export type Like = {
  id: number;
  userId: number;
  postId: number;
  user: User;
  post: Post;
  createdAt: Date | string;
  updatedAt: Date | string;
};
