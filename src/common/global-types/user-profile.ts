export type UserProfile = {
  id: number;
  bio: string;
  twitterUrl: string;
  instagramUrl: string;
  phoneNumber: string;
  facebookUrl: string;
  createdAt: Date | string;
  updatedAt: Date | string;
};
