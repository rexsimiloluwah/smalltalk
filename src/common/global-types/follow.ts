import { User } from "./user";

export type Follow = {
  id: number;
  userId: number;
  followerId: number;
  user: User;
  follower: User;
  createdAt: Date | string;
  updatedAt: Date | string;
};
