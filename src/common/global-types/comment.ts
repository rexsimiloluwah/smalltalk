import { Post } from "./post";
import { User } from "./user";

export type Comment = {
  id: number;
  content: string;
  postId: number;
  creatorId: number;
  post: Post;
  creator: User;
  createdAt: Date | string;
  updatedAt: Date | string;
};
