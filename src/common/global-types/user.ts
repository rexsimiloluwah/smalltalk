import { Follow } from "./follow";
import { UserProfile } from "./user-profile";

export type User = {
  id: number;
  email: string;
  username: string;
  firstname: string;
  lastname: string;
  profileImageUrl: string;
  emailVerified: boolean;
  posts?: [];
  followers: Array<Follow>;
  following: Array<Follow>;
  profile: UserProfile;
  createdAt: Date | string;
  updatedAt: Date | string;
};
