import { Comment } from "./comment";
import { Like } from "./like";
import { User } from "./user";

export type Post = {
  id: number;
  title: string;
  audioFileUrl: string;
  description: string;
  creatorId: number;
  creator: User;
  category: string;
  tags: string;
  comments: Comment[];
  likes: Like[];
  createdAt: Date | string;
  updatedAt: Date | string;
};
