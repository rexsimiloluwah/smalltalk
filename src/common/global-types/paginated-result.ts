export type PaginatedResult<T> = {
  data: T[];
  hasNextPage: number;
  totalPages: number;
  totalCount: number;
  currentPage: number;
};
