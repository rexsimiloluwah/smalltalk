import { gql } from "@apollo/client";

export const GET_AUTH_USER_FEED = gql`
  query ($args: FeedPaginationArgs!, $where: FeedWhereSelectInputDTO!) {
    userFeed(args: $args, where: $where) {
      hasNextPage
      totalPages
      totalCount
      currentPage
      data {
        id
        title
        description
        category
        audioFileUrl
        createdAt
        creator {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
        }
        likes {
          id
          createdAt
          user {
            id
            email
            username
            firstname
            lastname
          }
        }
        comments {
          id
          createdAt
          creator {
            id
            email
            username
            firstname
            lastname
          }
        }
      }
    }
  }
`;

export const GET_GUEST_USER_FEED = gql`
  query ($args: FeedPaginationArgs!, $where: FeedWhereSelectInputDTO!) {
    guestUserFeed(args: $args, where: $where) {
      hasNextPage
      totalPages
      totalCount
      currentPage
      data {
        id
        title
        description
        category
        audioFileUrl
        createdAt
        creator {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
        }
        likes {
          id
          createdAt
          user {
            id
            email
            username
            firstname
            lastname
          }
        }
        comments {
          id
          createdAt
          creator {
            id
            email
            username
            firstname
            lastname
          }
        }
      }
    }
  }
`;
