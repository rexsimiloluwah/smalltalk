import { gql } from "@apollo/client";

export const CREATE_POST_MUTATION = gql`
  mutation ($file: Upload!, $data: CreatePostArgs!) {
    createPost(data: $data, file: $file) {
      id
      title
      creatorId
      audioFileUrl
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_POST_MUTATION = gql`
  mutation ($where: PostWhereUniqueInputDTO!, $data: UpdatePostInputDTO!) {
    updatePost(where: $where, data: $data) {
      id
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_POST_MUTATION = gql`
  mutation ($where: PostWhereUniqueInputDTO!) {
    deletePost(where: $where) {
      id
      createdAt
      updatedAt
    }
  }
`;

export const GET_ALL_POSTS_ID = gql`
  query ($where: PostWhereUniqueInputDTO!) {
    findPosts(where: $where) {
      id
    }
  }
`;

export const GET_POST_UNIQUE = gql`
  query ($where: PostWhereUniqueInputDTO!) {
    post(where: $where) {
      id
      title
      description
      category
      audioFileUrl
      createdAt
      creator {
        id
        email
        firstname
        lastname
        username
        profileImageUrl
      }
      likes {
        id
        createdAt
        user {
          id
          email
          username
          firstname
          lastname
        }
      }
      comments {
        id
        content
        createdAt
        creator {
          id
          email
          username
          firstname
          lastname
        }
      }
    }
  }
`;

export const LIKE_POST = gql`
  mutation ($where: PostWhereUniqueInputDTO!) {
    likePost(where: $where) {
      id
      createdAt
      user {
        id
      }
    }
  }
`;

export const UNLIKE_POST = gql`
  mutation ($where: PostWhereUniqueInputDTO!) {
    unlikePost(where: $where) {
      id
      createdAt
      user {
        id
      }
    }
  }
`;

export const GET_TOPIC_UNIQUE = gql`
  query ($where: TopicWhereUniqueInputDTO!) {
    topic(where: $where) {
      id
      name
      createdAt
      posts {
        id
        title
        description
        category
        audioFileUrl
        createdAt
      }
    }
  }
`;

export const GET_POPULAR_TOPICS = gql`
  query {
    topics {
      id
      name
      createdAt
      posts {
        id
        title
        description
        category
        audioFileUrl
        createdAt
      }
    }
  }
`;
