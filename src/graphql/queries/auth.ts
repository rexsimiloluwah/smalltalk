import { gql } from "@apollo/client";

export const LOGIN_MUTATION = gql`
  mutation LoginMutation($data: LoginUserInputDTO!) {
    login(data: $data) {
      user {
        id
        email
        firstname
        username
        lastname
        createdAt
      }
      token {
        accessToken
        refreshToken
      }
    }
  }
`;

export const LOGIN_WITH_GOOGLE_MUTATION = gql`
  mutation LoginWithGoogleMutation($data: GoogleLoginUserInputDTO!) {
    loginWithGoogle(data: $data) {
      user {
        id
        email
        firstname
        username
        lastname
        createdAt
      }
      token {
        accessToken
        refreshToken
      }
    }
  }
`;

export const GET_AUTH_USER = gql`
  query {
    me {
      id
      email
      username
      firstname
      lastname
      createdAt
      updatedAt
      posts {
        id
        title
      }
      followers {
        user {
          id
          email
          username
          firstname
          lastname
        }
      }
      following {
        user {
          id
          email
          username
          firstname
          lastname
        }
      }
      profile {
        id
        bio
        twitterUrl
        phoneNumber
        instagramUrl
        facebookUrl
      }
    }
  }
`;

export const REFRESH_TOKEN_MUTATION = gql`
  mutation RefreshTokenMutation {
    refreshToken {
      accessToken
    }
  }
`;

export const REGISTER_USER_MUTATION = gql`
  mutation RegisterUserMutation($data: CreateUserInputDTO!) {
    register(data: $data) {
      id
      email
      username
      firstname
      lastname
      createdAt
    }
  }
`;
