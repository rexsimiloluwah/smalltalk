import { gql } from "@apollo/client";

export const GET_POST_COMMENT = gql`
  query ($where: CommentWhereUniqueInputDTO!) {
    comment(where: $where) {
      id
      content
      creator {
        id
        email
        username
        firstname
        lastname
      }
      createdAt
      updatedAt
    }
  }
`;
export const GET_POST_COMMENTS = gql`
  query ($where: CommentWhereUniqueInputDTO!) {
    comments(where: $where) {
      id
      content
      creator {
        id
        email
        username
        firstname
        lastname
      }
      createdAt
      updatedAt
    }
  }
`;

export const CREATE_POST_COMMENT = gql`
  mutation ($data: CreateCommentInputDTO!) {
    createComment(data: $data) {
      id
      content
      postId
      creatorId
      updatedAt
      createdAt
    }
  }
`;

export const UPDATE_POST_COMMENT = gql`
  mutation (
    $where: CommentWhereUniqueInputDTO!
    $data: UpdateCommentInputDTO!
  ) {
    updateComment(data: $data, where: $where) {
      id
      content
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_POST_COMMENT = gql`
  mutation ($where: CommentWhereUniqueInputDTO!) {
    deleteComment(where: $where) {
      id
      createdAt
      updatedAt
    }
  }
`;
