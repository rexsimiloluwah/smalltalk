import { gql } from "@apollo/client";

export const GET_USERS = gql`
  query ($where: UserWhereUniqueInputDTO!) {
    users(where: $where) {
      id
      email
      firstname
      lastname
      username
      profileImageUrl
      emailVerified
    }
  }
`;

export const GET_USER_UNIQUE = gql`
  query ($where: UserWhereUniqueInputDTO!) {
    user(where: $where) {
      id
      email
      firstname
      lastname
      username
      profileImageUrl
      emailVerified
      posts {
        id
        title
        description
        category
        audioFileUrl
        createdAt
        creator {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
        }
        likes {
          id
          createdAt
          user {
            id
            email
            username
            firstname
            lastname
          }
        }
        comments {
          id
          content
          createdAt
          creator {
            id
            email
            username
            firstname
            lastname
          }
        }
      }
      followers {
        id
        createdAt
        user {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
          emailVerified
        }
        follower {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
          emailVerified
        }
      }
      following {
        id
        createdAt
        user {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
          emailVerified
        }
        follower {
          id
          email
          firstname
          lastname
          username
          profileImageUrl
          emailVerified
        }
      }
      profile {
        id
        bio
        twitterUrl
        phoneNumber
        instagramUrl
        facebookUrl
      }
    }
  }
`;

export const FOLLOW_USER = gql`
  mutation ($where: UserWhereUniqueInputDTO!) {
    follow(where: $where) {
      id
      createdAt
      user {
        id
        email
        firstname
        lastname
        username
        profileImageUrl
        emailVerified
      }
    }
  }
`;

export const UNFOLLOW_USER = gql`
  mutation ($where: UserWhereUniqueInputDTO!) {
    unfollow(where: $where) {
      id
      createdAt
      user {
        id
        email
        firstname
        lastname
        username
        profileImageUrl
        emailVerified
      }
    }
  }
`;

export const UPDATE_USER = gql`
  mutation ($data: UpdateUserInputDTO!) {
    updateUser(data: $data) {
      id
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_USER_PROFILE = gql`
  mutation ($data: UpdateUserProfileInputDTO!) {
    updateUserProfile(data: $data) {
      id
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_USER_PASSWORD = gql`
  mutation ($data: UpdatePasswordInputDTO!) {
    updateUserPassword(data: $data) {
      id
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_USER = gql`
  mutation {
    deleteUser {
      id
    }
  }
`;
