import React from "react";
import { Navigate } from "react-router-dom";
import { getAccessTokenFromCookie } from "./lib/useAuth";

interface IGuestRouteProps {
  redirectPath?: string;
  children: React.ReactNode;
}

const GuestRoute: React.FC<IGuestRouteProps> = ({ redirectPath, children }) => {
  const token = getAccessTokenFromCookie();

  return token ? (
    <Navigate to={redirectPath as string} />
  ) : (
    <div>{children}</div>
  );
};

export default GuestRoute;

GuestRoute.defaultProps = {
  redirectPath: "/feed",
};
