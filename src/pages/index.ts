export { default as Home } from "./Home";
export { default as Login } from "./Login";
export { default as SignUp } from "./Signup";
export { default as Feed } from "./Feed";
export { default as CreatePost } from "./CreatePost";
export { default as UserProfile } from "./UserProfile";
export { default as UpdateAccount } from "./UpdateAccount";
export { default as PostDetail } from "./Post";
