import React from "react";
import { useState, useEffect } from "react";
import { useAuth } from "../lib/useAuth";
import { User } from "../common/global-types";
import { useParams } from "react-router-dom";
import { LeftSidebar, RightSidebar } from "../components/Feed";
import ProfileMain from "../components/User/ProfileMain";
import { GET_USER_UNIQUE } from "../graphql/queries/user";

const UserProfile = () => {
  const { authUser, createAuthApolloClient } = useAuth();
  const client = createAuthApolloClient();
  const [user, setUser] = useState<User | null>(null);
  const { username } = useParams();

  useEffect(() => {
    const getUser = async () => {
      const { data } = await client.query({
        query: GET_USER_UNIQUE,
        variables: { where: { username: username } },
      });

      setUser(data.user);
    };

    if (authUser !== null) {
      getUser();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authUser, username]);

  return (
    <div>
      <main className="flex font-uchen overflow-hidden max-h-screen">
        <LeftSidebar />
        <ProfileMain user={user as User} />
        <RightSidebar />
      </main>
    </div>
  );
};

export default UserProfile;
