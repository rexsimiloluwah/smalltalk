import React, { useState, useEffect } from "react";
import { LeftSidebar, Main, RightSidebar } from "../components/Feed";
import { Toaster } from "react-hot-toast";
import { GET_AUTH_USER_FEED } from "../graphql/queries/feed";
import { useSearchParams } from "react-router-dom";
import { SearchInputProvider } from "../context/SearchInputContext";
import { DEFAULT_FEED_PAGE_SIZE } from "../common/constants";
import { PaginatedResult, Post } from "../common/global-types";
import { useAuth } from "../lib/useAuth";

const Feed = () => {
  const { createAuthApolloClient, authUser } = useAuth();
  const [searchParams] = useSearchParams();
  const [feed, setFeed] = useState<PaginatedResult<Post> | null>(null);
  const topic = searchParams.get("topic") || "";
  const client = createAuthApolloClient();

  useEffect(() => {
    const getFeed = async () => {
      const { data } = await client.query({
        query: GET_AUTH_USER_FEED,
        variables: {
          args: {
            pageSize: DEFAULT_FEED_PAGE_SIZE,
          },
          where: {
            topic: topic,
          },
        },
      });
      setFeed(data.userFeed);
    };

    if (authUser !== null) {
      getFeed();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authUser, topic]);

  return (
    <div>
      <main className="flex font-uchen overflow-hidden max-h-screen">
        <SearchInputProvider>
          <Toaster />
          <LeftSidebar />
          <Main feed={feed} topic={topic} />
          <RightSidebar />
        </SearchInputProvider>
      </main>
    </div>
  );
};

export default Feed;
