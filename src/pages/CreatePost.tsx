import React from "react";
import { useNavigate } from "react-router-dom";
import { CreatePost } from "../components/Post";
import { FiArrowLeft } from "react-icons/fi";

const CreatePostPage = () => {
  const navigate = useNavigate();
  return (
    <div>
      <main className="relative">
        <button
          onClick={() => navigate(-1)}
          type="button"
          className="shadow-lg absolute top-5 left-5 p-3 rounded-full bg-gradient-dark opacity-90 hover:opacity-100 group transition-all"
        >
          <FiArrowLeft
            size={24}
            className="text-white group-hover:-translate-x-1 duration-200"
          />
        </button>
        <CreatePost />
      </main>
    </div>
  );
};

export default CreatePostPage;
