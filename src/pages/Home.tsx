import React from "react";
import { Navbar } from "../components/shared";
import { Hero, About, StayUpdated, Footer } from "../components/LandingPage";

const Home = () => {
  return (
    <>
      <main>
        <Navbar />
        <Hero />
        <About />
        <StayUpdated />
        <Footer />
      </main>
    </>
  );
};

export default Home;
