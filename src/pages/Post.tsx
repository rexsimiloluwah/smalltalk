import React from "react";
import { useEffect, useState } from "react";
import { Post } from "../common/global-types";
import { LeftSidebar } from "../components/Feed";
import { GET_POST_UNIQUE } from "../graphql/queries/post";
import { PostView } from "../components/Post";
import { Toaster } from "react-hot-toast";
import { useParams } from "react-router-dom";
import { useAuth } from "../lib/useAuth";

const PostDetail = () => {
  const { id } = useParams();
  const { createAuthApolloClient } = useAuth();
  const client = createAuthApolloClient();
  const [post, setPost] = useState<Post | null>(null);

  useEffect(() => {
    const getPost = async () => {
      const { data } = await client.query({
        query: GET_POST_UNIQUE,
        variables: { where: { id: Number(id) } },
      });

      setPost(data.post);
    };

    getPost();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <>
      <Toaster />
      <div>
        <main className="flex font-uchen overflow-hidden max-h-screen">
          <LeftSidebar />
          <PostView post={post} />
        </main>
      </div>
    </>
  );
};

export default PostDetail;
