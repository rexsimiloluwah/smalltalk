import React from "react";
import { LeftSidebar } from "../components/Feed";
import { UpdateUserContainer } from "../components/User";

const UpdateAccount = () => {
  return (
    <div>
      <main className="flex font-uchen overflow-hidden max-h-screen">
        <LeftSidebar />
        <UpdateUserContainer />
      </main>
    </div>
  );
};

export default UpdateAccount;
