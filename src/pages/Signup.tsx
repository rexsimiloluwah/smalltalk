import React from "react";
import { SignupForm } from "../components/Auth";
import { useNavigate } from "react-router-dom";
import { FiArrowLeft } from "react-icons/fi";
import { BiPlus } from "react-icons/bi";

const SignUp = () => {
  const navigate = useNavigate();

  return (
    <div>
      <main>
        <div className="flex justify-between">
          <div className="bg-gradient-light w-[50%] min-h-screen hidden md:flex font-uchen">
            <div className="min-h-screen flex justify-center px-12 flex-col space-y-8">
              <h1 className="text-5xl font-bold break-words text-gray-700">
                Join The Small Talk Community
              </h1>
              <div className="flex space-x-4 items-center">
                <div className="avatar__group flex">
                  <div className="border-4 w-24 h-24 border-indigo-500 rounded-full p-2 bg-white justify-center flex items-center z-10">
                    <img
                      alt="user-girl"
                      src={"/images/memoji-girl.png"}
                      className="w-full h-full"
                    />
                  </div>
                  <div className="border-4 w-24 h-24 border-indigo-500 rounded-full p-2 bg-white justify-center flex items-center -ml-5 z-10">
                    <img
                      alt="user-man"
                      src={"/images/memoji-man.png"}
                      className="w-full h-full"
                    />
                  </div>
                  <div className="border-4 w-24 h-24 border-indigo-500 rounded-full p-2 bg-white justify-center flex items-center -ml-5 z-10">
                    <img
                      alt="user-dog"
                      src={"/images/memoji-dog.png"}
                      className="w-full h-full"
                    />
                  </div>
                </div>

                <div className="p-5 bg-white rounded-full flex justify-center items-center">
                  <BiPlus size={32} className="text-lg" />
                </div>
                <div className="border-4 border-fuchsia-500 bg-white w-20 h-20 rounded-full flex justify-center items-center show">
                  <span>YOU</span>
                </div>
              </div>
            </div>
          </div>
          <div className="md:w-[50%] w-full relative">
            <button
              onClick={() => navigate(-1)}
              type="button"
              className="shadow-lg absolute top-5 left-5 p-3 rounded-full bg-gradient-dark opacity-90 hover:opacity-100 group transition-all"
            >
              <FiArrowLeft
                size={24}
                className="text-white group-hover:-translate-x-1 duration-200"
              />
            </button>
            <SignupForm />
          </div>
        </div>
      </main>
    </div>
  );
};

export default SignUp;
